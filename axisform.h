#ifndef AXISFORM_H
#define AXISFORM_H

#include <QWidget>
#include <gp_Pnt.hxx>

namespace Ui {
class axisForm;
}

class axisForm : public QWidget
{
    Q_OBJECT

public:
    explicit axisForm(QWidget *parent = nullptr);
    ~axisForm();
    void fireAddNewAxis();
signals:
        void sendAddNewAxis(gp_Pnt aOrigin,
                        QString xGridValues,
                        QString yGridValues,
                        QString zGridValues,
                        QString xGridLabels,
                        QString yGridLabels,
                        QString zGridLabels);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::axisForm *ui;
};

#endif // AXISFORM_H

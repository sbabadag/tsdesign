#include "occObjects.h"
#include <AIS_InteractiveContext.hxx>
#include <vector>
#include <QFile>
#include <QStringListModel>
#include <QStandardItemModel>
#include <QVector>
#include <QMessageBox>
#include <QTextStream>
#include <Geom_TrimmedCurve.hxx>
#include <GC_MakeSegment.hxx>
#include <TopoDS_Edge.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <TopoDS_Wire.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <TopoDS_Face.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <QElapsedTimer>
#include <QDebug>



struct Profile
{
    QString Name;
    double b;
    double h;
    double t;
    double w;
    double ix;
    double iy;
    double sx;
    double sy;

};


void QOCC_ipe_Profile::createShape()
{

    QElapsedTimer timer;
    timer.start();
    //
    Profile *P;
    QVector<Profile*> Profiles;
    QFile IPE_Profile_File;



    IPE_Profile_File.setFileName("IPE.txt");
    if(!IPE_Profile_File.open(QIODevice::ReadOnly)) {
        QMessageBox::information(nullptr, "error", IPE_Profile_File.errorString());
    }

    QTextStream in(&IPE_Profile_File);

    while(!in.atEnd()) {
        QString line = in.readLine();
        QStringList fields = line.split("	");
        P = new Profile;
        P->Name = "IPE"+fields[0];
        P->b = fields[2].toDouble();
        P->h = fields[1].toDouble();
        P->t = fields[3].toDouble();
        P->w = fields[4].toDouble();
        P->ix = fields[11].toDouble();
        P->iy = fields[13].toDouble();
        P->sx = fields[12].toDouble();
        P->sy = fields[14].toDouble();
        Profiles.push_back(P);
    }

    IPE_Profile_File.close();

    for (int i = 0; i < Profiles.count(); ++i)

     {
        if (Profiles[i]->Name == profileName)
        {
            P = Profiles[i];
        }
    }

    //
    gp_Pnt pt0(-P->b / 2, -P->h / 2, 0);
    gp_Pnt pt1(P->b / 2, -P->h / 2, 0);
    gp_Pnt pt2(P->b / 2, -P->h / 2 + P->t, 0);
    gp_Pnt pt3(P->t / 2, -P->h / 2 + P->t, 0);
    gp_Pnt pt4(P->t / 2, P->h / 2 - P->t, 0);
    gp_Pnt pt5(P->b / 2, P->h / 2 - P->t, 0);
    gp_Pnt pt6(P->b / 2, P->h / 2, 0);
    gp_Pnt pt7(-P->b / 2, P->h / 2, 0);
    gp_Pnt pt8(-P->b / 2, P->h / 2 - P->t, 0);
    gp_Pnt pt9(-P->t / 2, P->h / 2 - P->t, 0);
    gp_Pnt pt10(-P->t / 2, -P->h / 2 + P->t, 0);
    gp_Pnt pt11(-P->b / 2, -P->h / 2 + P->t, 0);
    //
    Handle(Geom_TrimmedCurve) aSegment1 = GC_MakeSegment(pt0, pt1);
    Handle(Geom_TrimmedCurve) aSegment2 = GC_MakeSegment(pt1, pt2);
    Handle(Geom_TrimmedCurve) aSegment3 = GC_MakeSegment(pt2, pt3);
    Handle(Geom_TrimmedCurve) aSegment4 = GC_MakeSegment(pt3, pt4);
    Handle(Geom_TrimmedCurve) aSegment5 = GC_MakeSegment(pt4, pt5);
    Handle(Geom_TrimmedCurve) aSegment6 = GC_MakeSegment(pt5, pt6);
    Handle(Geom_TrimmedCurve) aSegment7 = GC_MakeSegment(pt6, pt7);
    Handle(Geom_TrimmedCurve) aSegment8 = GC_MakeSegment(pt7, pt8);
    Handle(Geom_TrimmedCurve) aSegment9 = GC_MakeSegment(pt8, pt9);
    Handle(Geom_TrimmedCurve) aSegment10 = GC_MakeSegment(pt9, pt10);
    Handle(Geom_TrimmedCurve) aSegment11 = GC_MakeSegment(pt10, pt11);
    Handle(Geom_TrimmedCurve) aSegment12 = GC_MakeSegment(pt11, pt0);
    //
//    Handle(Geom_TrimmedCurve) aSegment13 = GC_MakeSegment(pt12, pt13);

    TopoDS_Edge anEdge1 = BRepBuilderAPI_MakeEdge(aSegment1);
    TopoDS_Edge anEdge2 = BRepBuilderAPI_MakeEdge(aSegment2);
    TopoDS_Edge anEdge3 = BRepBuilderAPI_MakeEdge(aSegment3);
    TopoDS_Edge anEdge4 = BRepBuilderAPI_MakeEdge(aSegment4);
    TopoDS_Edge anEdge5 = BRepBuilderAPI_MakeEdge(aSegment5);
    TopoDS_Edge anEdge6 = BRepBuilderAPI_MakeEdge(aSegment6);
    TopoDS_Edge anEdge7 = BRepBuilderAPI_MakeEdge(aSegment7);
    TopoDS_Edge anEdge8 = BRepBuilderAPI_MakeEdge(aSegment8);
    TopoDS_Edge anEdge9 = BRepBuilderAPI_MakeEdge(aSegment9);
    TopoDS_Edge anEdge10 = BRepBuilderAPI_MakeEdge(aSegment10);
    TopoDS_Edge anEdge11 = BRepBuilderAPI_MakeEdge(aSegment11);
    TopoDS_Edge anEdge12 = BRepBuilderAPI_MakeEdge(aSegment12);
    //
//    TopoDS_Edge anEdge13 = BRepBuilderAPI_MakeEdge(aSegment13);

    //
    TopoDS_Wire threadingWire1 = BRepBuilderAPI_MakeWire(anEdge1);
    TopoDS_Wire threadingWire2 = BRepBuilderAPI_MakeWire(anEdge2);
    TopoDS_Wire threadingWire3 = BRepBuilderAPI_MakeWire(anEdge3);
    TopoDS_Wire threadingWire4 = BRepBuilderAPI_MakeWire(anEdge4);
    TopoDS_Wire threadingWire5 = BRepBuilderAPI_MakeWire(anEdge5);
    TopoDS_Wire threadingWire6 = BRepBuilderAPI_MakeWire(anEdge6);
    TopoDS_Wire threadingWire7 = BRepBuilderAPI_MakeWire(anEdge7);
    TopoDS_Wire threadingWire8 = BRepBuilderAPI_MakeWire(anEdge8);
    TopoDS_Wire threadingWire9 = BRepBuilderAPI_MakeWire(anEdge9);
    TopoDS_Wire threadingWire10 = BRepBuilderAPI_MakeWire(anEdge10);
    TopoDS_Wire threadingWire11 = BRepBuilderAPI_MakeWire(anEdge11);
    TopoDS_Wire threadingWire12 = BRepBuilderAPI_MakeWire(anEdge12);
    //
 //   TopoDS_Wire threadingWire13 = BRepBuilderAPI_MakeWire(anEdge13);

    //
    BRepBuilderAPI_MakeWire mkWire;

    mkWire.Add(threadingWire1);
    mkWire.Add(threadingWire2);
    mkWire.Add(threadingWire3);
    mkWire.Add(threadingWire4);
    mkWire.Add(threadingWire5);
    mkWire.Add(threadingWire6);
    mkWire.Add(threadingWire7);
    mkWire.Add(threadingWire8);
    mkWire.Add(threadingWire9);
    mkWire.Add(threadingWire10);
    mkWire.Add(threadingWire11);
    mkWire.Add(threadingWire12);
    //
    TopoDS_Wire myWireProfile = mkWire.Wire();
    TopoDS_Face myFaceProfile = BRepBuilderAPI_MakeFace(myWireProfile);

    gp_Trsf Tr,Tr1;
    gp_Ax3 oldAxis(gp_Pnt(0, 0, 0), gp_Dir(gp_Vec(gp_Pnt(0,0,0),gp_Pnt(0,0,1))));
 //   gp_Ax3 newAxis(p1, gp_Dir(gp_Vec(p1,p2)));

 //   profileAxis = newAxis;

    Tr.SetDisplacement(oldAxis, profileAxis);

    BRepBuilderAPI_Transform xform1(myFaceProfile, Tr);


    Tr1.SetRotation(gp_Ax1(p1,gp_Dir(gp_Vec(p1,p2))),DEG(90));

    BRepBuilderAPI_Transform xform2(xform1.Shape(), Tr1);


    gp_Vec aPrismVec(p1, p2);
    TopoDS_Shape IPE_Profile = BRepPrimAPI_MakePrism(xform2.Shape(), aPrismVec);

    shape = new AIS_Shape(IPE_Profile);


    shape->SetColor(color);

    Context->Display(shape,myUpdateViewer);


    qDebug() << "The extrude operation took" << timer.elapsed() << "milliseconds";
}

void QOCC_ipe_Profile::setOrientation(float deg)
{
    gp_Trsf Tr;

    Tr.SetRotation(gp_Ax1(p1,gp_Dir(gp_Vec(p1,p2))),(deg-orientation)*(M_PI/180));
    BRepBuilderAPI_Transform xform(shape->Shape(),Tr);
    shape->SetShape(xform.Shape());
    QOCC_Object::setOrientation(deg);


}

void QOCC_ipe_Profile::setOffset(gp_Pnt2d offset)
{
    gp_Ax3 newAxis;
    QOCC_Object::setOffset(offset);






}


void QOCC_Object::setOffset(gp_Pnt2d offset)
{
    this->offset = offset;
}













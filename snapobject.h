#ifndef SNAPOBJECT_H
#define SNAPOBJECT_H

#include <AIS_Shape.hxx>
#include <AIS_InteractiveContext.hxx>
#include <AIS_ViewController.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Shape.hxx>
#include <Prs3d_PointAspect.hxx>
#include <QVector>
#include <QMouseEvent>
#include <QPoint>
#include <BRepBuilderAPI_Copy.hxx>




enum snapMode {
    snapMode_None,
    snapMode_End,
    snapMode_Mid,
    snapMode_Nearest,
    snapMode_Perpendecular,
    snapMode_Center,
    snapMode_Intersection
};

typedef QFlags<snapMode> snapType;

Q_DECLARE_FLAGS(theSnapType, snapMode)

Q_DECLARE_OPERATORS_FOR_FLAGS(theSnapType)

 class snapObject : public QObject
{
     Q_OBJECT
     void detectObject(QPoint P);
 public:
    snapObject(QObject *parent, Handle(AIS_InteractiveContext) Context,Handle(V3d_View) aView) : QObject(parent)
    {
        theContext=Context;theView=aView;
        selectedShape.Nullify();
//
        TopoDS_Vertex V1 = BRepBuilderAPI_MakeVertex(gp_Pnt(0,0,0));

        theSnapObject = new AIS_Shape(V1);

        // Set the vertex shape, color, and size
         color = Quantity_Color(Quantity_NOC_RED1);
        Handle_Prs3d_PointAspect myPointAspect=new Prs3d_PointAspect(Aspect_TOM_O_PLUS   ,color,snapObjectSize);
        theSnapObject->Attributes()->SetPointAspect(myPointAspect);
 //       theContext->Display(theSnapObject,false);
    }
    void draw();
    void hide();
    gp_Pnt getWorldPoint(QPoint QP);
    void setGridSnapPoints(QVector<gp_Pnt*> agridPoints) {gridPoints=agridPoints;}
    bool getSnappedPoint(gp_Pnt& P)
    {
        P = snappedPoint;
        return snapStatus;
    }
    void setSelectedShape(TopoDS_Shape& aselectedShape)
    {
        selectedShape = aselectedShape;
        auto a = selectedShape.ShapeType();
     //   getShapeSnap(snappedPoint);
    }
    void activate() {active=true;}
    void deactivate() {active=false;hide();}
    void setSnapMode(theSnapType aSnapMode) {mySnapMode = aSnapMode;}
    TopoDS_Edge getSelectedEdge(Standard_Real& length);
    void setColor(Quantity_Color aColor);
private:
    bool                            getGridSnap(gp_Pnt& P);
    bool                            getShapeSnap(gp_Pnt &P);
    Handle(AIS_InteractiveContext)  theContext;
    Handle(V3d_View)                theView;
    Handle(AIS_Shape)               theSnapObject;
    TopoDS_Shape                    selectedShape;
    TopoDS_Edge                     selectedEdge;
    QVector<gp_Pnt*>                gridPoints;
    bool                            hidden = true;
    bool                            snapStatus = false;
    bool                            active = false;
    gp_Pnt                          RealCoordinate;
    gp_Pnt                          snappedPoint;
    Standard_Real                   snapTolerance = 500;
    theSnapType                     mySnapMode = snapMode_End;
    Standard_Real                   snapObjectSize = 5;
    Quantity_Color                  color;


public slots:
    void mouseMoveEvent(QMouseEvent * theEvent);


 };


#endif // SNAPOBJECT_H

#ifndef SNAPMANAGER_H
#define SNAPMANAGER_H


#include <AIS_InteractiveContext.hxx>
#include <V3d_View.hxx>
#include <QPoint>
#include <AIS_ViewController.hxx>
#include <AIS_InteractiveObject.hxx>
#include <Graphic3d_Vec2.hxx>
#include "occ_helper_functions.h"
#include <AIS_Shape.hxx>
#include <TopoDS_Shape.hxx>
#include <gp_Pnt.hxx>
//


enum snapMode {
         snapMode_None,
         snapMode_End,
         snapMode_Mid,
         snapMode_Nearest,
         snapMode_Perpendecular,
         snapMode_Center,
         snapMode_Intersection
};

class SnapManager
{
public:
    SnapManager(const Handle(AIS_InteractiveContext) Context,Handle(V3d_View) View);
    ~SnapManager() {}


private:
    Handle(AIS_InteractiveContext) theContext;
    Handle(V3d_View) theView;
    void receiveMouseMove(const int theFlags, const QPoint thePoint);
    void receiveMouseUp(const int theFlags, const QPoint thePoint);
    void receiveSelectedObjectChange(Handle(AIS_Shape) &selectedShape ) {}
    bool DetectSnap(gp_Pnt dpoint);
    //
    gp_Pnt firstPoint ;
    gp_Pnt lastPoint;
    gp_Pnt curSPt ;
    gp_Pnt snappedPoint;

    gp_Pnt realMouseCoordinate;
    Handle (AIS_Shape) snapObject;
    Handle (AIS_Shape) tmpSelectedObj;
    snapMode theSnapStatus;

    bool snapStatus = false;
    int lClickCount = 0;
    int rClickCount = 0;
    int mClickCount = 0;
    float snpRectSize = 7;

    AIS_ViewController* VK;


};

#endif // SNAPMANAGER_H

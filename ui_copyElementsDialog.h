/********************************************************************************
** Form generated from reading UI file 'copyElementsDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COPYELEMENTSDIALOG_H
#define UI_COPYELEMENTSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_copy_elements_dialog
{
public:
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QGroupBox *groupBox;
    QLabel *label;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLabel *label_2;
    QLineEdit *lineEdit_3;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *lineEdit_4;
    QToolButton *toolButton;
    QToolButton *toolButton_2;

    void setupUi(QWidget *copy_elements_dialog)
    {
        if (copy_elements_dialog->objectName().isEmpty())
            copy_elements_dialog->setObjectName(QStringLiteral("copy_elements_dialog"));
        copy_elements_dialog->resize(235, 235);
        copy_elements_dialog->setMaximumSize(QSize(235, 235));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/Copy-object-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        copy_elements_dialog->setWindowIcon(icon);
        pushButton = new QPushButton(copy_elements_dialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(30, 200, 80, 22));
        pushButton_2 = new QPushButton(copy_elements_dialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(120, 200, 80, 22));
        groupBox = new QGroupBox(copy_elements_dialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(20, 20, 201, 161));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 30, 71, 16));
        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(90, 30, 50, 21));
        lineEdit_2 = new QLineEdit(groupBox);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(90, 60, 50, 21));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 60, 71, 16));
        lineEdit_3 = new QLineEdit(groupBox);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(90, 90, 50, 21));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 90, 71, 16));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 120, 71, 16));
        lineEdit_4 = new QLineEdit(groupBox);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(90, 120, 30, 21));
        toolButton = new QToolButton(groupBox);
        toolButton->setObjectName(QStringLiteral("toolButton"));
        toolButton->setGeometry(QRect(150, 90, 24, 21));
        toolButton_2 = new QToolButton(groupBox);
        toolButton_2->setObjectName(QStringLiteral("toolButton_2"));
        toolButton_2->setGeometry(QRect(130, 120, 61, 21));

        retranslateUi(copy_elements_dialog);

        QMetaObject::connectSlotsByName(copy_elements_dialog);
    } // setupUi

    void retranslateUi(QWidget *copy_elements_dialog)
    {
        copy_elements_dialog->setWindowTitle(QApplication::translate("copy_elements_dialog", "Kopyalama", Q_NULLPTR));
        pushButton->setText(QApplication::translate("copy_elements_dialog", "Kopyala", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("copy_elements_dialog", "Kapat", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("copy_elements_dialog", "Koordinatlar", Q_NULLPTR));
        label->setText(QApplication::translate("copy_elements_dialog", "X distance", Q_NULLPTR));
        lineEdit->setText(QApplication::translate("copy_elements_dialog", "0", Q_NULLPTR));
        lineEdit_2->setText(QApplication::translate("copy_elements_dialog", "0", Q_NULLPTR));
        label_2->setText(QApplication::translate("copy_elements_dialog", "Y distance", Q_NULLPTR));
        lineEdit_3->setText(QApplication::translate("copy_elements_dialog", "0", Q_NULLPTR));
        label_3->setText(QApplication::translate("copy_elements_dialog", "Z distance", Q_NULLPTR));
        label_4->setText(QApplication::translate("copy_elements_dialog", "Adet", Q_NULLPTR));
        lineEdit_4->setText(QApplication::translate("copy_elements_dialog", "1", Q_NULLPTR));
        toolButton->setText(QApplication::translate("copy_elements_dialog", "...", Q_NULLPTR));
        toolButton_2->setText(QApplication::translate("copy_elements_dialog", "Clear", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class copy_elements_dialog: public Ui_copy_elements_dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COPYELEMENTSDIALOG_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "myglview.h"
#include "occSnippets.h"
#include "BRepAlgo_Section.hxx"
#include <TopoDS_Solid.hxx>
#include <BRepPrimAPI_MakeHalfSpace.hxx>
#include <ShapeUpgrade_UnifySameDomain.hxx>
#include <BRepTools_WireExplorer.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <GC_MakePlane.hxx>
#include <gp_Trsf.hxx>
#include <occObjects.h>
#include "copyElementsDialog.h"
#include <STEPControl_Writer.hxx>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
      , ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    axis_dialog = new axisForm(this->parentWidget());
    axis_dialog->setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);


    connect(ui->openGLWidget,&MyGLView::sendMouseReceivedCoordinates,
            this,&MainWindow::getReceivedMouseCoordinates);
    //

    connect(ui->openGLWidget,&MyGLView::sendButtonStatus,
            this,&MainWindow::getSetButtonStatus);
    //
    connect(axis_dialog,&axisForm::sendAddNewAxis,
            this,&MainWindow::getAddNewGrid);
    //
    setGeometry(QRect(0,0,1000,1000));

    ui->openGLWidget->Bar = ui->statusbar;

    ui->verticalComboBox->addItem("Middle",NULL);
    ui->verticalComboBox->addItem("Top",NULL);
    ui->verticalComboBox->addItem("Below",NULL);
    //
    ui->horizontalComboBox->addItem("Middle",NULL);
    ui->horizontalComboBox->addItem("Left",NULL);
    ui->horizontalComboBox->addItem("Right",NULL);
    //
    ui->rotationComboBox->addItem("Front",NULL);
    ui->rotationComboBox->addItem("Top",NULL);
    ui->rotationComboBox->addItem("Back",NULL);
    ui->rotationComboBox->addItem("Below",NULL);

    ui->openGLWidget->addItemInPopup(ui->actionKopyala);
    ui->openGLWidget->addItemInPopup(ui->actionMove_Object);
    ui->openGLWidget->addItemInPopup(ui->actionRotate_Object);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionLine_triggered()
{
//
}

void MainWindow::on_actionFront_View_triggered()
{
    ui->openGLWidget->setViewType(ViewType_Front);
}

void MainWindow::on_actionFit_All_triggered()
{
    ui->openGLWidget->fitAll();

}

void MainWindow::on_actionBack_View_triggered()
{
    ui->openGLWidget->setViewType(ViewType_Back);
}

void MainWindow::on_actionTop_View_triggered()
{
    ui->openGLWidget->setViewType(ViewType_Top);
}

void MainWindow::on_actionBottom_View_triggered()
{
    ui->openGLWidget->setViewType(ViewType_Bottom);
}

void MainWindow::on_actionLeft_View_triggered()
{
    ui->openGLWidget->setViewType(ViewType_Left);
}

void MainWindow::on_actionRight_View_triggered()
{
    ui->openGLWidget->setViewType(ViewType_Right);
}

void MainWindow::on_action3D_View_triggered()
{
    ui->openGLWidget->setViewType(ViewType_3D);
}

void MainWindow::on_actionRotate_Object_triggered()
{
    Prf->setOrientation(90);
}

void MainWindow::on_actionGrid_triggered()
{
    //drawOnPlane(ui->openGLWidget->getContext(),ui->openGLWidget->myView,gp_Pln(gp_Pnt(100,100,100),gp_Dir(gp_Vec(gp_Pnt(0,0,0),gp_Pnt(100,100,100)))),gp_Pnt2d(0,0),gp_Pnt2d(0,1000));
    //drawAxis(ui->openGLWidget->getContext(),"0 10000 20000","0 6000 12000 18000 24000 30000","","");
}

void MainWindow::getReceivedMouseCoordinates(gp_Pnt &p)
{
    ui->statusbar->showMessage(QString("X : %1 - Y: %2 - Z: %3").arg(p.X()).arg(p.Y()).arg(p.Z()));
}

void MainWindow::getSetButtonStatus(int button, bool status)
{
    // set button status
    switch (button) {
    case 0:
    {
        ui->actionDraw_beam->setCheckable(true);
        ui->actionDraw_beam->setChecked(status);
    }
        break;
    case 1:

        break;
    default:
        break;
    }
}

void MainWindow::getAddNewGrid(gp_Pnt aOrigin,QString xGridValues,QString yGridValues,QString zGridValues,QString xGridLabels,QString yGridLabels,QString zGridLabels)
{
    ui->openGLWidget->addGrid( aOrigin,
                               xGridValues,
                               yGridValues,
                               zGridValues,
                               xGridLabels,
                               yGridLabels,
                               zGridLabels);
}

void MainWindow::on_actionWork_Plane_triggered()
{
 //   ui->openGLWidget->myViewer->SetPrivilegedPlane(gp_Ax3(gp_Pnt(0,0,0),gp_Dir(gp_Vec(gp_Pnt(0,0,0),gp_Pnt(0,1,0)))));
}

void MainWindow::on_actionKat_triggered()
{
    ui->openGLWidget->getContext()->SetDisplayMode(AIS_Shaded, Standard_True);
}

void MainWindow::on_actionWireframe_triggered()
{
    ui->openGLWidget->getContext()->SetDisplayMode(AIS_WireFrame, Standard_True);

}

void MainWindow::on_actionGizli_izgi_triggered()
{
    ui->openGLWidget->getContext()->SetDisplayMode(AIS_WireFrame, Standard_True);

}

void MainWindow::on_action_k_triggered()
{
    exit(EXIT_FAILURE);
}

void MainWindow::on_actionKopyala_triggered()
{
    // copy dialog
    ui->openGLWidget->setMode(CurAction3d_CopyObjects);

    ui->openGLWidget->copy_dialog->showNormal();
}

void MainWindow::on_actionKaydet_triggered()
{
    BRep_Builder builder;
    TopoDS_Compound compound;
    builder.MakeCompound(compound);

    foreach (QOCC_Object *o,  ui->openGLWidget->Objects) {
        builder.Add(compound, o->getAISShape()->Shape());
    }

    STEPControl_Writer writer;
    writer.Transfer(compound,STEPControl_AsIs);
    writer.Write("D:/demo1.stp");
}

void MainWindow::on_actionDraw_Column_triggered()
{
    // draw Column

}

void MainWindow::on_actionDraw_beam_triggered()
{
    // draw Beam
    ui->openGLWidget->setMode(CurAction3d_DrawElement);
    ui->actionDraw_beam->setChecked(true);
}

void MainWindow::on_actionDENEME_triggered()
{
    ui->openGLWidget->setPreviligedPlaneToCamera();
}

void MainWindow::on_actionAdd_axis_triggered()
{
    axis_dialog->showNormal();
}

void MainWindow::on_actionDeleteObjects_triggered()
{
    ui->openGLWidget->deleteSelectedObjects();
}

void MainWindow::on_actionByTwoPoints_triggered()
{
    ui->openGLWidget->setMode(CurAction3d_SetWorkPlane2Points);
    ui->actionByTwoPoints->setCheckable(true);
    ui->actionByTwoPoints->setChecked(true);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    ui->openGLWidget->keyPressEvent(event);
}

void MainWindow::on_actionViewToWorkplane_triggered()
{
       ui->openGLWidget->setViewToWorkPlane();
}

#ifndef COPY_ELEMENTS_DIALOG_H
#define COPY_ELEMENTS_DIALOG_H

#include <QWidget>
#include <gp_Pnt.hxx>

namespace Ui {
class copy_elements_dialog;
}

class copy_elements_dialog : public QWidget
{
    Q_OBJECT

public:
    explicit copy_elements_dialog(QWidget *parent = nullptr);
    ~copy_elements_dialog();
    void fireCopyRequestSignal() { emit sendCopyRequest(copyParameters,no_o); }
    void firePickPointSignal();
    void setPickedPoint(gp_Pnt P);

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();
    void on_toolButton_triggered(QAction *arg1);

    void on_toolButton_clicked();

    void on_toolButton_2_clicked();

signals:
    void sendCopyRequest(gp_Pnt &p,int no);
    void sendPickPointRequest();


private:
    Ui::copy_elements_dialog *ui;
    gp_Pnt copyParameters;
    int no_o;
    gp_Pnt pickedPoint;
};

#endif // COPY_ELEMENTS_DIALOG_H

/********************************************************************************
** Form generated from reading UI file 'axisform.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AXISFORM_H
#define UI_AXISFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_axisForm
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_8;
    QLineEdit *lineEdit_9;
    QLabel *label_7;
    QLineEdit *lineEdit_8;
    QLabel *label_9;
    QLineEdit *lineEdit_7;
    QWidget *layoutWidget_2;
    QGridLayout *gridLayout_2;
    QLabel *label_4;
    QLineEdit *lineEdit_4;
    QLabel *label_5;
    QLineEdit *lineEdit_5;
    QLabel *label_6;
    QLineEdit *lineEdit_6;
    QWidget *layoutWidget_3;
    QHBoxLayout *_2;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLabel *label_10;
    QWidget *widget;
    QGridLayout *gridLayout_3;
    QLabel *label;
    QLineEdit *lineEdit;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QLabel *label_3;
    QLineEdit *lineEdit_3;

    void setupUi(QWidget *axisForm)
    {
        if (axisForm->objectName().isEmpty())
            axisForm->setObjectName(QStringLiteral("axisForm"));
        axisForm->resize(672, 341);
        layoutWidget = new QWidget(axisForm);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 240, 455, 24));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(20, 0, 0, 0);
        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 0, 0, 1, 1);

        lineEdit_9 = new QLineEdit(layoutWidget);
        lineEdit_9->setObjectName(QStringLiteral("lineEdit_9"));

        gridLayout->addWidget(lineEdit_9, 0, 1, 1, 1);

        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 0, 2, 1, 1);

        lineEdit_8 = new QLineEdit(layoutWidget);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));

        gridLayout->addWidget(lineEdit_8, 0, 3, 1, 1);

        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 0, 4, 1, 1);

        lineEdit_7 = new QLineEdit(layoutWidget);
        lineEdit_7->setObjectName(QStringLiteral("lineEdit_7"));

        gridLayout->addWidget(lineEdit_7, 0, 5, 1, 1);

        layoutWidget_2 = new QWidget(axisForm);
        layoutWidget_2->setObjectName(QStringLiteral("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(20, 140, 631, 80));
        gridLayout_2 = new QGridLayout(layoutWidget_2);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(20, 0, 0, 0);
        label_4 = new QLabel(layoutWidget_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_2->addWidget(label_4, 0, 0, 1, 1);

        lineEdit_4 = new QLineEdit(layoutWidget_2);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));

        gridLayout_2->addWidget(lineEdit_4, 0, 1, 1, 1);

        label_5 = new QLabel(layoutWidget_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_2->addWidget(label_5, 1, 0, 1, 1);

        lineEdit_5 = new QLineEdit(layoutWidget_2);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));

        gridLayout_2->addWidget(lineEdit_5, 1, 1, 1, 1);

        label_6 = new QLabel(layoutWidget_2);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_2->addWidget(label_6, 2, 0, 1, 1);

        lineEdit_6 = new QLineEdit(layoutWidget_2);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));

        gridLayout_2->addWidget(lineEdit_6, 2, 1, 1, 1);

        layoutWidget_3 = new QWidget(axisForm);
        layoutWidget_3->setObjectName(QStringLiteral("layoutWidget_3"));
        layoutWidget_3->setGeometry(QRect(210, 300, 168, 24));
        _2 = new QHBoxLayout(layoutWidget_3);
        _2->setObjectName(QStringLiteral("_2"));
        _2->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(layoutWidget_3);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        _2->addWidget(pushButton);

        pushButton_2 = new QPushButton(layoutWidget_3);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        _2->addWidget(pushButton_2, 0, Qt::AlignHCenter);

        label_10 = new QLabel(axisForm);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(289, 9, 111, 16));
        widget = new QWidget(axisForm);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(20, 31, 631, 80));
        gridLayout_3 = new QGridLayout(widget);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(20, 0, 0, 0);
        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_3->addWidget(label, 0, 0, 1, 1);

        lineEdit = new QLineEdit(widget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        gridLayout_3->addWidget(lineEdit, 0, 1, 1, 1);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_3->addWidget(label_2, 1, 0, 1, 1);

        lineEdit_2 = new QLineEdit(widget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        gridLayout_3->addWidget(lineEdit_2, 1, 1, 1, 1);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_3->addWidget(label_3, 2, 0, 1, 1);

        lineEdit_3 = new QLineEdit(widget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        gridLayout_3->addWidget(lineEdit_3, 2, 1, 1, 1);


        retranslateUi(axisForm);

        QMetaObject::connectSlotsByName(axisForm);
    } // setupUi

    void retranslateUi(QWidget *axisForm)
    {
        axisForm->setWindowTitle(QApplication::translate("axisForm", "Form", Q_NULLPTR));
        label_8->setText(QApplication::translate("axisForm", "X Origin : ", Q_NULLPTR));
        lineEdit_9->setText(QApplication::translate("axisForm", "0", Q_NULLPTR));
        label_7->setText(QApplication::translate("axisForm", "Y Origin : ", Q_NULLPTR));
        lineEdit_8->setText(QApplication::translate("axisForm", "0", Q_NULLPTR));
        label_9->setText(QApplication::translate("axisForm", "Z Origin : ", Q_NULLPTR));
        lineEdit_7->setText(QApplication::translate("axisForm", "0", Q_NULLPTR));
        label_4->setText(QApplication::translate("axisForm", "X Labels : ", Q_NULLPTR));
        lineEdit_4->setText(QApplication::translate("axisForm", "A B C D E", Q_NULLPTR));
        label_5->setText(QApplication::translate("axisForm", "Y Labels : ", Q_NULLPTR));
        lineEdit_5->setText(QApplication::translate("axisForm", "1 2 3 4 5 6", Q_NULLPTR));
        label_6->setText(QApplication::translate("axisForm", "Z Labels : ", Q_NULLPTR));
        lineEdit_6->setText(QApplication::translate("axisForm", "0 7 9", Q_NULLPTR));
        pushButton->setText(QApplication::translate("axisForm", "Add Grid", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("axisForm", "Cancel", Q_NULLPTR));
        label_10->setText(QApplication::translate("axisForm", "Add a Grid Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("axisForm", "X Axes : ", Q_NULLPTR));
        lineEdit->setText(QApplication::translate("axisForm", "0 10000 20000 30000 40000", Q_NULLPTR));
        label_2->setText(QApplication::translate("axisForm", "Y Axes : ", Q_NULLPTR));
        lineEdit_2->setText(QApplication::translate("axisForm", "0 6000 12000 18000 24000 30000", Q_NULLPTR));
        label_3->setText(QApplication::translate("axisForm", "Z Axes : ", Q_NULLPTR));
        lineEdit_3->setText(QApplication::translate("axisForm", "0 7000 9000", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class axisForm: public Ui_axisForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AXISFORM_H

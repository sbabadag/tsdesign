#ifndef MYGLVIEW_H
#define MYGLVIEW_H

#include <QGLWidget>
#include <QMainWindow>

#include <AIS_InteractiveContext.hxx>
#include <vector>
#include <QMessageBox>
#include "occObjects.h"
#include <AIS_RubberBand.hxx>
#include <AIS_ViewController.hxx>
#include <AIS_Line.hxx>
#include <AIS_Manipulator.hxx>
#include "customgrid.h"
#include "snapobject.h"
#include "axisform.h"
#include "linetracker.h"
#include "copyElementsDialog.h"


enum CurrentAction3d
{
    CurAction3d_Nothing,
    CurAction3d_DynamicZooming,
    CurAction3d_WindowZooming,
    CurAction3d_DynamicPanning,
    CurAction3d_GlobalPanning,
    CurAction3d_DynamicRotation,
    CurAction3d_TracklineDrawing,
    CurAction3d_CopyObjects,
    CurAction3d_MoveObjects,
    CurAction3d_DrawElement,
    CurAction3d_SetWorkPlane2Points,
    CurAction3d_GetpickPoint
};

enum clickStatus
{
    clickStatusNone,
    clickStatusLeftFirstClick,
    clickStatusLeftLastClick,
    clickStatusRightFirstClick,
    clickStatusRightLastClick,
    clickStatusMidFirstClick,
    clickStatusMidLastClick
};

enum ViewType
{
    ViewType_3D,
    ViewType_Front,
    ViewType_Back,
    ViewType_Top,
    ViewType_Bottom,
    ViewType_Left,
    ViewType_Right
};

using namespace std;

class QMenu;
class QRubberBand;

//! adapted a QGLWidget for OpenCASCADE viewer.
class MyGLView : public QGLWidget
{
    Q_OBJECT

public:
    //! constructor.
    MyGLView(QWidget* parent);
    void *ui;
    QStatusBar *Bar;
    vector<gp_Pnt> points;
    Handle(V3d_View) myView;
    Handle(V3d_Viewer) myViewer;
    QVector<QOCC_Object*> selectedObjects;
    QVector<QOCC_Object*> Objects;
    copy_elements_dialog* copy_dialog;


    Handle(AIS_InteractiveContext)& getContext() ;
    void handleSelectionChanged();
    void deleteSelectedObjects();
    gp_Pnt selectionChanged(void);
    void addObject(QOCC_Object *o);
    void fireMouseSignal() { emit sendMouseReceivedCoordinates(RM_Coordinate);}
    void fireButtonStatus(int button, bool status) {emit sendButtonStatus(button,status);}
    void copyObjects(QVector<QOCC_Object*> Objects,gp_Pnt cparameters,int no);
    void setMode(CurrentAction3d aMode);
    void fireSnapMouseSignal(QMouseEvent * theEvent) {emit sendMouseMoveEvent(theEvent);}
    void setViewType(ViewType aViewType);
    void addGrid(gp_Pnt aOrigin,QString xGridValues,QString yGridValues,QString zGridValues,QString xGridLabels,QString yGridLabels,QString zGridLabels);
    void setWorkPlane(gp_Ax3 aPlane);
    void setWorkPlane(gp_Pnt p1, gp_Pnt p2, gp_Pnt p3);
    void setWorkPlane(gp_Pnt p1,gp_Pnt p2);
    void setViewToWorkPlane();
    void keyPressEvent(QKeyEvent* event) ;
    void setPreviligedPlaneToCamera();
    void addItemInPopup(QAction* theMenu);


public slots:
    //! operations for the view.
    void pan(void);
    void fitAll(void);
    void reset(void);
    void zoom(void);
    void rotate(void);
    void getpickPointRequest();
    void getCopyRequest(gp_Pnt &p,int no);


signals:
    void ChangeDistances(QString G);
    void sendMouseReceivedCoordinates(gp_Pnt &p);
    void sendButtonStatus(int button, bool status);
    void sendMouseMoveEvent(QMouseEvent * theEvent);

protected:
    // Paint events.
    virtual void paintEvent(QPaintEvent* theEvent);
    virtual void resizeEvent(QResizeEvent* theEvent);

    // Mouse events.
    virtual void mousePressEvent(QMouseEvent* theEvent);
    virtual void mouseReleaseEvent(QMouseEvent* theEvent);
    virtual void mouseMoveEvent(QMouseEvent * theEvent);
    virtual void wheelEvent(QWheelEvent * theEvent);

    // Button events.
    virtual void onLButtonDown(const int theFlags, const QPoint thePoint);
    virtual void onMButtonDown(const int theFlags, const QPoint thePoint);
    virtual void onRButtonDown(const int theFlags, const QPoint thePoint);
    virtual void onMouseWheel(const int theFlags, const int theDelta, const QPoint thePoint);
    virtual void onLButtonUp(const int theFlags, const QPoint thePoint);
    virtual void onMButtonUp(const int theFlags, const QPoint thePoint);
    virtual void onRButtonUp(const int theFlags, const QPoint thePoint);
    virtual void onMouseMove(const int theFlags, const QPoint thePoint);
    // Popup menu.

protected:
    void init(void);
    void popup(const int x, const int y);
    void dragEvent(const int x, const int y);
    void inputEvent(const int x, const int y);
    void moveEvent(const int x, const int y);
    void multiMoveEvent(const int x, const int y);
    void multiDragEvent(const int x, const int y);
    void multiInputEvent(const int x, const int y);
    void panByMiddleButton(const QPoint& thePoint);

private:
    void OnMouseHover(Standard_Real x, Standard_Real y,gp_Pnt &P);
    void DrawRectangleRubberBand (Standard_Boolean theToDraw);

    //! the occ viewer.

    //! the occ view.

    //! the occ context.
    Handle(AIS_InteractiveContext) myContext;

    //! save the mouse position.
    Standard_Integer myXmin;
    Standard_Integer myYmin;
    Standard_Integer myXmax;
    Standard_Integer myYmax;

    //! the mouse current mode.
    CurrentAction3d myCurrentMode;
    clickStatus myClickStatus;
    bool snapStatus = false;
    int lClickCount = 0;
    int rClickCount = 0;
    int mClickCount = 0;
    float snpRectSize = 20;

    //! save the degenerate mode state.
    Standard_Boolean myDegenerateModeIsOn;

    //! rubber rectangle for the mouse selection.
    QRubberBand* myRectBand;
    QMessageBox msgBox;
    customgrid* myGrid;
    snapObject* mySnapObject;
    lineTracker* tracker;

    Handle (AIS_Line)  trackLine;
    Handle (AIS_Shape) theSnapObject;
    Handle (AIS_Shape) tmpSelectedObj;
    Handle (AIS_RubberBand) myRubberBand;


    gp_Pnt firstPoint ;
    gp_Pnt lastPoint;
    gp_Pnt curSPt ;
    gp_Pnt snappedPoint;
    gp_Pnt RM_Coordinate;

    gp_Ax3 workPlane;

    QMenu* MyMenu;



};
#endif // MYGLVIEW_H

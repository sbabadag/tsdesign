#include "customgrid.h"
#include <TopoDS_Builder.hxx>
#include <TopoDS_Compound.hxx>
#include <algorithm>
#include <Geom_Point.hxx>
#include <Geom_CartesianPoint.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <AIS_TextLabel.hxx>
#include <BRepBuilderAPI_GTransform.hxx>
#include <gp_Lin2d.hxx>
#include <BRepBuilderAPI_MakeEdge2d.hxx>
#include "occSnippets.h"


#define DEG(ang) ang*M_PI/180
Standard_Real Xmax=0,Ymax=0,Zmax=0;


customgrid::customgrid(Handle(AIS_InteractiveContext) Context,QString xGridValues,
                       QString yGridValues,
                       QString zGridValues,
                       QString xGridLabels,
                       QString yGridLabels,
                       QString zGridLabels)
{
    theContext = Context;
    lineProps = new Prs3d_LineAspect(color,lineType,lineThickness);

    setGrid(origin, xGridValues,
             yGridValues,
             zGridValues,
             xGridLabels,
             yGridLabels,
            zGridLabels);
//

}


void customgrid::createObjects()
{
    Standard_Real XMAX,YMAX,ZMAX,a;

    XMAX = Xmax + 1000;
    YMAX = Ymax + 1000;
    ZMAX = Zmax + 1000;
    a          = -1000;


  // create XOY grid lines and labels
  TopoDS_Builder* builderXOY = new TopoDS_Builder();
  TopoDS_Builder* builderXOZ = new TopoDS_Builder();
  TopoDS_Builder* builderYOZ = new TopoDS_Builder();




  TopoDS_Compound* shapeXOY = new TopoDS_Compound();
  TopoDS_Compound* shapeXOZ = new TopoDS_Compound();
  TopoDS_Compound* shapeYOZ = new TopoDS_Compound();

  builderXOY->MakeCompound(*shapeXOY);
  builderXOZ->MakeCompound(*shapeXOZ);
  builderYOZ->MakeCompound(*shapeYOZ);


  foreach (CGrid* c, xLines)
  {
      c->sPt = gp_Pnt(c->sPt.X(),a,0);
      c->ePt = gp_Pnt(c->ePt.X(),YMAX,0);

      BRepBuilderAPI_MakePolygon gline(c->sPt,c->ePt);
      builderXOY->Add(*shapeXOY,gline.Shape());
  }


  foreach (CGrid* c, yLines)
  {
      c->sPt = gp_Pnt(a,c->sPt.Y(),0);
      c->ePt = gp_Pnt(XMAX,c->ePt.Y(),0);
      BRepBuilderAPI_MakePolygon gline(c->sPt,c->ePt);
      builderXOY->Add(*shapeXOY,gline.Shape());
  }

  XOY_objects =  new AIS_Shape(*shapeXOY);

  XOY_objects->Attributes()->SetWireAspect(lineProps);
  //finished adding x O y lines






  foreach (CGrid* c, xLines)
  {
      c->sPt = gp_Pnt(c->sPt.X(),a,0);
      c->ePt = gp_Pnt(c->ePt.X(),ZMAX,0);
      BRepBuilderAPI_MakePolygon gline(c->sPt,c->ePt);
      builderXOZ->Add(*shapeXOZ,gline.Shape());
  }


  foreach (CGrid* c, zLines)
  {
      c->sPt = gp_Pnt(a,c->sPt.Y(),0);
      c->ePt = gp_Pnt(XMAX,c->ePt.Y(),0);
      BRepBuilderAPI_MakePolygon gline(c->sPt,c->ePt);
      builderXOZ->Add(*shapeXOZ,gline.Shape());
  }

  XOZ_objects =  new AIS_Shape(*shapeXOZ);

  XOZ_objects->Attributes()->SetWireAspect(lineProps);

  // rotate the object from XY plane to XZ plane

  gp_Trsf T;

  T.SetRotation(gp::OX(),DEG(90));

  BRepBuilderAPI_GTransform  anAISObj(XOZ_objects->Shape(),T);

  XOZ_objects->Set(anAISObj.Shape());


  //finished adding x O z lines








  foreach (CGrid* c, yLines)
  {
      c->sPt = gp_Pnt(c->sPt.Y(),a,0);
      c->ePt = gp_Pnt(c->ePt.Y(),ZMAX,0);
      BRepBuilderAPI_MakePolygon gline(c->sPt,c->ePt);
      builderYOZ->Add(*shapeYOZ,gline.Shape());
  }


  foreach (CGrid* c, zLines)
  {
      c->sPt = gp_Pnt(a,c->sPt.Y(),0);
      c->ePt = gp_Pnt(YMAX,c->ePt.Y(),0);
      BRepBuilderAPI_MakePolygon gline(c->sPt,c->ePt);
      builderYOZ->Add(*shapeYOZ,gline.Shape());
  }

  YOZ_objects =  new AIS_Shape(*shapeYOZ);

  YOZ_objects->Attributes()->SetWireAspect(lineProps);

  // rotate the object from XY plane to YZ plane

  gp_Trsf T1;


  T1.SetRotation(gp::OX(),DEG(90));

  BRepBuilderAPI_GTransform  anAISObj1(YOZ_objects->Shape(),T1);

  YOZ_objects->Set(anAISObj1.Shape());

  T1.SetRotation(gp::OZ(),DEG(90));

  BRepBuilderAPI_GTransform  anAISObj2(YOZ_objects->Shape(),T1);

  YOZ_objects->Set(anAISObj2.Shape());
  //finished adding y O z lines

  draw();

}





void customgrid::setGrid(gp_Pnt aOrigin, QString xGridValues, QString yGridValues, QString zGridValues, QString xGridLabels, QString yGridLabels, QString zGridLabels)
{
    QStringList *XV,*YV,*ZV;
    QStringList *XLn,*YLn,*ZLn;

    QVector<Standard_Real> Xn,Yn,Zn;

    gp_Pnt pt1,pt2;

    deleteAll();

    origin = aOrigin;

    XV = new (QStringList);
    YV = new (QStringList);
    ZV = new (QStringList);

    XLn = new (QStringList);
    YLn = new (QStringList);
    ZLn = new (QStringList);




    *XV = xGridValues.split(QRegExp(" "), QString::SkipEmptyParts);
    *YV = yGridValues.split(QRegExp(" "), QString::SkipEmptyParts);
    *ZV = zGridValues.split(QRegExp(" "), QString::SkipEmptyParts);

    *XLn = xGridLabels.split(QRegExp(" "), QString::SkipEmptyParts);
    *YLn = yGridLabels.split(QRegExp(" "), QString::SkipEmptyParts);
    *ZLn = zGridLabels.split(QRegExp(" "), QString::SkipEmptyParts);

    for (int i=0;i<XV->size();i++)
    {
        Xn.push_back(XV->at(i).toFloat());
    };

    for (int i=0;i<YV->size();i++)
    {
        Yn.push_back(YV->at(i).toFloat());
    };

    for (int i=0;i<ZV->size();i++)
    {
        Zn.push_back(ZV->at(i).toFloat());
    };

    Xmax = *std::max_element(Xn.constBegin(), Xn.constEnd());
    Ymax = *std::max_element(Yn.constBegin(), Yn.constEnd());
    Zmax = *std::max_element(Zn.constBegin(), Zn.constEnd());


    for (int x=0;x<Xn.size();x++)
    {
        pt1 = gp_Pnt(Xn[x],0,0);
        pt2 = gp_Pnt(Xn[x],0,0);
        //
        CGrid* c = new CGrid(XLn->at(x),pt1,pt2);
        xLines.push_back(c);
    }
    //
    for (int y=0;y<Yn.size();y++)
    {
        pt1 = gp_Pnt(0,Yn[y],0);
        pt2 = gp_Pnt(0,Yn[y],0);
        //
        CGrid* c = new CGrid(YLn->at(y),pt1,pt2);
        yLines.push_back(c);
    }
    //
    for (int z=0;z<Zn.size();z++)
    {
        pt1 = gp_Pnt(0,Zn[z],0);
        pt2 = gp_Pnt(0,Zn[z],0);
        //
        CGrid* c = new CGrid(ZLn->at(z),pt1,pt2);
        zLines.push_back(c);
    }

    gridPoints = GetGridPoints(origin,xGridValues,yGridValues,zGridValues);
    createObjects();
}

void customgrid::draw()
{

    hide();

    switch (planeToDraw) {
    case drawPlaneXOY:
        theContext->Display(XOY_objects,AIS_Shaded ,-1,true,false);
        break;
    case drawPlaneXOZ:
        theContext->Display(XOZ_objects,AIS_Shaded ,-1,true,false);
        break;
    case drawPlaneYOZ:
        theContext->Display(YOZ_objects,AIS_Shaded ,-1,true,false);
        break;
    default:
        theContext->Display(XOY_objects,AIS_Shaded ,-1,true,false);
        break;
    }

/*
    TopTools_ListOfShape aShapeList;





    foreach (CGrid* c, xLines) {
        Handle(AIS_TextLabel) aText = new AIS_TextLabel();
        aText->SetText (c->Label.toStdString().c_str());
        aText->SetPosition (c->sPt);
        aText->SetAngle (0);
        aText->SetColor (Quantity_NOC_BLUE1);
        aText->SetFontAspect (Font_FA_Regular);
        aText->SetFont ("Arial");
        aText->SetHeight (300);
        aText->SetHJustification (Graphic3d_HTA_LEFT);
        aText->SetVJustification (Graphic3d_VTA_BOTTOM);
        aText->SetZoomable (Standard_True);
        theContext->Display(aText,Standard_False);
//        Handle(AIS_Shape) anAISObj = Handle(AIS_Shape)::DownCast(aText);
 //       aShapeList.Append(anAISObj->Shape());
    }
    //
    foreach (CGrid* c, yLines) {
        Handle(AIS_TextLabel) aText = new AIS_TextLabel();
        aText->SetText (c->Label.toStdString().c_str());
        aText->SetPosition (c->sPt);
        aText->SetAngle (0);
        aText->SetColor (Quantity_NOC_BLUE1);
        aText->SetFontAspect (Font_FA_Regular);
        aText->SetFont ("Arial");
        aText->SetHeight (300);
        aText->SetHJustification (Graphic3d_HTA_LEFT);
        aText->SetVJustification (Graphic3d_VTA_BOTTOM);
        aText->SetZoomable (Standard_True);
        theContext->Display(aText,Standard_False);
 //       Handle(AIS_Shape) anAISObj = Handle(AIS_Shape)::DownCast(aText);
 //       aShapeList.Append(anAISObj->Shape());

    }

    foreach (CGrid* c, zLines) {
        Handle(AIS_TextLabel) aText = new AIS_TextLabel();
        aText->SetText (c->Label.toStdString().c_str());
        aText->SetPosition (c->sPt);
        aText->SetAngle (0);
        aText->SetColor (Quantity_NOC_BLUE1);
        aText->SetFontAspect (Font_FA_Regular);
        aText->SetFont ("Arial");
        aText->SetHeight (300);
        aText->SetHJustification (Graphic3d_HTA_LEFT);
        aText->SetVJustification (Graphic3d_VTA_BOTTOM);
        aText->SetZoomable (Standard_True);
        theContext->Display(aText,Standard_False);
 //       Handle(AIS_Shape) anAISObj = Handle(AIS_Shape)::DownCast(aText);
 //       aShapeList.Append(anAISObj->Shape());

    }
*/

}

void customgrid::hide()
{
    theContext->Remove(XOY_objects,true);
    theContext->Remove(XOZ_objects,true);
    theContext->Remove(YOZ_objects,true);
}

void customgrid::deleteAll()
{
    gridPoints.clear();
    xLines.clear();
    yLines.clear();
    zLines.clear();

    theContext->Erase(XOY_objects,true);
    theContext->Erase(XOZ_objects,true);
    theContext->Erase(YOZ_objects,true);
}



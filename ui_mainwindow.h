/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>
#include <myglview.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionLine;
    QAction *actionFront_View;
    QAction *actionBack_View;
    QAction *actionTop_View;
    QAction *actionBottom_View;
    QAction *actionLeft_View;
    QAction *actionRight_View;
    QAction *action3D_View;
    QAction *actionFit_All;
    QAction *actionRotate_Object;
    QAction *actionGrid;
    QAction *actionWork_Plane;
    QAction *actionKat;
    QAction *actionWireframe;
    QAction *actionGizli_izgi;
    QAction *action_k;
    QAction *actionKopyala;
    QAction *actionKaydet;
    QAction *actionMove_Object;
    QAction *actionDraw_Column;
    QAction *actionDraw_beam;
    QAction *actionDENEME;
    QAction *actionAdd_axis;
    QAction *actionSaveAs;
    QAction *actionMove;
    QAction *actionDeleteObjects;
    QAction *actionByTwoPoints;
    QAction *actionViewToWorkplane;
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    MyGLView *openGLWidget;
    QMenuBar *menubar;
    QMenu *menuDosya;
    QMenu *menuAyarlar;
    QMenu *menuG_r_nt;
    QMenu *menuModifiye;
    QMenu *menuAdd_Object;
    QMenu *menuView;
    QMenu *menuViewStyle;
    QMenu *menuWorkPlane;
    QStatusBar *statusbar;
    QToolBar *toolBar;
    QToolBar *toolBar_2;
    QToolBar *toolBar_3;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents_3;
    QTabWidget *tabWidget;
    QWidget *tab;
    QToolBox *toolBox;
    QWidget *page;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_2;
    QLabel *label_4;
    QLabel *label_3;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_3;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_4;
    QLabel *label;
    QToolButton *toolButton;
    QToolButton *toolButton_2;
    QWidget *page_2;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_3;
    QLabel *label_5;
    QLineEdit *lineEdit_5;
    QLineEdit *lineEdit_7;
    QLabel *label_6;
    QLineEdit *lineEdit_6;
    QLineEdit *lineEdit_8;
    QWidget *page_3;
    QGroupBox *groupBox;
    QWidget *layoutWidget2;
    QGridLayout *gridLayout_7;
    QLineEdit *lineEdit_30;
    QLabel *label_24;
    QLabel *label_23;
    QCheckBox *checkBox_2;
    QLabel *label_25;
    QLabel *label_26;
    QLineEdit *lineEdit_32;
    QLineEdit *lineEdit_28;
    QLineEdit *lineEdit_29;
    QLineEdit *lineEdit_27;
    QLabel *label_27;
    QLineEdit *lineEdit_31;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox;
    QWidget *layoutWidget3;
    QGridLayout *gridLayout_6;
    QComboBox *verticalComboBox;
    QLineEdit *lineEdit_9;
    QLabel *label_11;
    QLabel *label_10;
    QLineEdit *lineEdit_10;
    QLineEdit *lineEdit_13;
    QLineEdit *lineEdit_12;
    QLabel *label_9;
    QCheckBox *checkBox_7;
    QLineEdit *lineEdit_11;
    QLabel *label_8;
    QComboBox *rotationComboBox;
    QLabel *label_7;
    QComboBox *horizontalComboBox;
    QCheckBox *checkBox_8;
    QCheckBox *checkBox_9;
    QCheckBox *checkBox_10;
    QCheckBox *checkBox_11;
    QPushButton *pushButton;
    QWidget *tab_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(846, 1106);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(800, 0));
        MainWindow->setMaximumSize(QSize(16777215, 16777215));
        MainWindow->setMouseTracking(true);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/a.ico"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        actionLine = new QAction(MainWindow);
        actionLine->setObjectName(QStringLiteral("actionLine"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/line.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLine->setIcon(icon1);
        actionFront_View = new QAction(MainWindow);
        actionFront_View->setObjectName(QStringLiteral("actionFront_View"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/Front-view-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionFront_View->setIcon(icon2);
        actionBack_View = new QAction(MainWindow);
        actionBack_View->setObjectName(QStringLiteral("actionBack_View"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/Back-view-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionBack_View->setIcon(icon3);
        actionTop_View = new QAction(MainWindow);
        actionTop_View->setObjectName(QStringLiteral("actionTop_View"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/Top-view-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionTop_View->setIcon(icon4);
        actionBottom_View = new QAction(MainWindow);
        actionBottom_View->setObjectName(QStringLiteral("actionBottom_View"));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/Bottom-view-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionBottom_View->setIcon(icon5);
        actionLeft_View = new QAction(MainWindow);
        actionLeft_View->setObjectName(QStringLiteral("actionLeft_View"));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/Left-view-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLeft_View->setIcon(icon6);
        actionRight_View = new QAction(MainWindow);
        actionRight_View->setObjectName(QStringLiteral("actionRight_View"));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/Right-view-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRight_View->setIcon(icon7);
        action3D_View = new QAction(MainWindow);
        action3D_View->setObjectName(QStringLiteral("action3D_View"));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/3D-view-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        action3D_View->setIcon(icon8);
        actionFit_All = new QAction(MainWindow);
        actionFit_All->setObjectName(QStringLiteral("actionFit_All"));
        actionRotate_Object = new QAction(MainWindow);
        actionRotate_Object->setObjectName(QStringLiteral("actionRotate_Object"));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/Rotate-object-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRotate_Object->setIcon(icon9);
        actionGrid = new QAction(MainWindow);
        actionGrid->setObjectName(QStringLiteral("actionGrid"));
        actionWork_Plane = new QAction(MainWindow);
        actionWork_Plane->setObjectName(QStringLiteral("actionWork_Plane"));
        actionKat = new QAction(MainWindow);
        actionKat->setObjectName(QStringLiteral("actionKat"));
        actionWireframe = new QAction(MainWindow);
        actionWireframe->setObjectName(QStringLiteral("actionWireframe"));
        actionGizli_izgi = new QAction(MainWindow);
        actionGizli_izgi->setObjectName(QStringLiteral("actionGizli_izgi"));
        action_k = new QAction(MainWindow);
        action_k->setObjectName(QStringLiteral("action_k"));
        actionKopyala = new QAction(MainWindow);
        actionKopyala->setObjectName(QStringLiteral("actionKopyala"));
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icons/Copy-object-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionKopyala->setIcon(icon10);
        actionKaydet = new QAction(MainWindow);
        actionKaydet->setObjectName(QStringLiteral("actionKaydet"));
        actionMove_Object = new QAction(MainWindow);
        actionMove_Object->setObjectName(QStringLiteral("actionMove_Object"));
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/icons/Move-object-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionMove_Object->setIcon(icon11);
        actionDraw_Column = new QAction(MainWindow);
        actionDraw_Column->setObjectName(QStringLiteral("actionDraw_Column"));
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/icons/Draw-column-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDraw_Column->setIcon(icon12);
        actionDraw_beam = new QAction(MainWindow);
        actionDraw_beam->setObjectName(QStringLiteral("actionDraw_beam"));
        actionDraw_beam->setCheckable(true);
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/icons/Draw-beam-32x32-QT.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDraw_beam->setIcon(icon13);
        actionDENEME = new QAction(MainWindow);
        actionDENEME->setObjectName(QStringLiteral("actionDENEME"));
        actionDENEME->setIcon(icon);
        actionAdd_axis = new QAction(MainWindow);
        actionAdd_axis->setObjectName(QStringLiteral("actionAdd_axis"));
        actionSaveAs = new QAction(MainWindow);
        actionSaveAs->setObjectName(QStringLiteral("actionSaveAs"));
        actionMove = new QAction(MainWindow);
        actionMove->setObjectName(QStringLiteral("actionMove"));
        actionDeleteObjects = new QAction(MainWindow);
        actionDeleteObjects->setObjectName(QStringLiteral("actionDeleteObjects"));
        actionByTwoPoints = new QAction(MainWindow);
        actionByTwoPoints->setObjectName(QStringLiteral("actionByTwoPoints"));
        actionViewToWorkplane = new QAction(MainWindow);
        actionViewToWorkplane->setObjectName(QStringLiteral("actionViewToWorkplane"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        openGLWidget = new MyGLView(centralwidget);
        openGLWidget->setObjectName(QStringLiteral("openGLWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(1);
        sizePolicy1.setHeightForWidth(openGLWidget->sizePolicy().hasHeightForWidth());
        openGLWidget->setSizePolicy(sizePolicy1);
        openGLWidget->setMinimumSize(QSize(0, 0));
        openGLWidget->setMaximumSize(QSize(16777215, 16777215));
        openGLWidget->setSizeIncrement(QSize(1, 1));
        openGLWidget->setMouseTracking(true);
        openGLWidget->setFocusPolicy(Qt::NoFocus);
        openGLWidget->setLayoutDirection(Qt::LeftToRight);

        gridLayout->addWidget(openGLWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 846, 21));
        menuDosya = new QMenu(menubar);
        menuDosya->setObjectName(QStringLiteral("menuDosya"));
        menuAyarlar = new QMenu(menubar);
        menuAyarlar->setObjectName(QStringLiteral("menuAyarlar"));
        menuG_r_nt = new QMenu(menuAyarlar);
        menuG_r_nt->setObjectName(QStringLiteral("menuG_r_nt"));
        menuModifiye = new QMenu(menubar);
        menuModifiye->setObjectName(QStringLiteral("menuModifiye"));
        menuAdd_Object = new QMenu(menubar);
        menuAdd_Object->setObjectName(QStringLiteral("menuAdd_Object"));
        menuView = new QMenu(menubar);
        menuView->setObjectName(QStringLiteral("menuView"));
        menuViewStyle = new QMenu(menuView);
        menuViewStyle->setObjectName(QStringLiteral("menuViewStyle"));
        menuWorkPlane = new QMenu(menuView);
        menuWorkPlane->setObjectName(QStringLiteral("menuWorkPlane"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        toolBar_2 = new QToolBar(MainWindow);
        toolBar_2->setObjectName(QStringLiteral("toolBar_2"));
        MainWindow->addToolBar(Qt::LeftToolBarArea, toolBar_2);
        toolBar_3 = new QToolBar(MainWindow);
        toolBar_3->setObjectName(QStringLiteral("toolBar_3"));
        MainWindow->addToolBar(Qt::BottomToolBarArea, toolBar_3);
        dockWidget = new QDockWidget(MainWindow);
        dockWidget->setObjectName(QStringLiteral("dockWidget"));
        dockWidget->setMinimumSize(QSize(300, 35));
        dockWidgetContents_3 = new QWidget();
        dockWidgetContents_3->setObjectName(QStringLiteral("dockWidgetContents_3"));
        tabWidget = new QTabWidget(dockWidgetContents_3);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 300, 967));
        tabWidget->setMinimumSize(QSize(300, 0));
        tabWidget->setStyleSheet(QStringLiteral("selection-background-color: rgb(150, 150, 150);"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        toolBox = new QToolBox(tab);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        toolBox->setGeometry(QRect(0, 0, 300, 421));
        toolBox->setMinimumSize(QSize(250, 0));
        toolBox->setStyleSheet(QStringLiteral("color = rgb(255, 255, 255)"));
        toolBox->setFrameShape(QFrame::StyledPanel);
        toolBox->setLineWidth(1);
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        page->setGeometry(QRect(0, 0, 298, 335));
        layoutWidget = new QWidget(page);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(7, 10, 281, 108));
        gridLayout_2 = new QGridLayout(layoutWidget);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setHorizontalSpacing(10);
        gridLayout_2->setContentsMargins(2, 0, 5, 0);
        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_2->addWidget(label_4, 3, 0, 1, 1);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 2, 0, 1, 1);

        lineEdit = new QLineEdit(layoutWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Maximum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
        lineEdit->setSizePolicy(sizePolicy2);

        gridLayout_2->addWidget(lineEdit, 0, 1, 1, 1);

        lineEdit_3 = new QLineEdit(layoutWidget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        gridLayout_2->addWidget(lineEdit_3, 2, 1, 1, 1);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        lineEdit_2 = new QLineEdit(layoutWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        gridLayout_2->addWidget(lineEdit_2, 1, 1, 1, 1);

        lineEdit_4 = new QLineEdit(layoutWidget);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));

        gridLayout_2->addWidget(lineEdit_4, 3, 1, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        toolButton = new QToolButton(layoutWidget);
        toolButton->setObjectName(QStringLiteral("toolButton"));

        gridLayout_2->addWidget(toolButton, 1, 2, 1, 1);

        toolButton_2 = new QToolButton(layoutWidget);
        toolButton_2->setObjectName(QStringLiteral("toolButton_2"));

        gridLayout_2->addWidget(toolButton_2, 2, 2, 1, 1);

        toolBox->addItem(page, QStringLiteral("General"));
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        page_2->setGeometry(QRect(0, 0, 298, 335));
        layoutWidget1 = new QWidget(page_2);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(4, 10, 291, 52));
        gridLayout_3 = new QGridLayout(layoutWidget1);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setHorizontalSpacing(4);
        gridLayout_3->setContentsMargins(0, 0, 5, 0);
        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_3->addWidget(label_5, 0, 0, 1, 1);

        lineEdit_5 = new QLineEdit(layoutWidget1);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));

        gridLayout_3->addWidget(lineEdit_5, 0, 1, 1, 1);

        lineEdit_7 = new QLineEdit(layoutWidget1);
        lineEdit_7->setObjectName(QStringLiteral("lineEdit_7"));

        gridLayout_3->addWidget(lineEdit_7, 0, 2, 1, 1);

        label_6 = new QLabel(layoutWidget1);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_3->addWidget(label_6, 1, 0, 1, 1);

        lineEdit_6 = new QLineEdit(layoutWidget1);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));

        gridLayout_3->addWidget(lineEdit_6, 1, 1, 1, 1);

        lineEdit_8 = new QLineEdit(layoutWidget1);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));

        gridLayout_3->addWidget(lineEdit_8, 1, 2, 1, 1);

        toolBox->addItem(page_2, QStringLiteral("Numbering Series"));
        page_3 = new QWidget();
        page_3->setObjectName(QStringLiteral("page_3"));
        page_3->setGeometry(QRect(0, 0, 298, 335));
        groupBox = new QGroupBox(page_3);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(0, 150, 291, 171));
        layoutWidget2 = new QWidget(groupBox);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(0, 30, 281, 101));
        gridLayout_7 = new QGridLayout(layoutWidget2);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        gridLayout_7->setVerticalSpacing(4);
        gridLayout_7->setContentsMargins(10, 0, 10, 0);
        lineEdit_30 = new QLineEdit(layoutWidget2);
        lineEdit_30->setObjectName(QStringLiteral("lineEdit_30"));

        gridLayout_7->addWidget(lineEdit_30, 3, 3, 1, 1);

        label_24 = new QLabel(layoutWidget2);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_24, 0, 3, 1, 1);

        label_23 = new QLabel(layoutWidget2);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_23, 0, 1, 1, 1);

        checkBox_2 = new QCheckBox(layoutWidget2);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setChecked(true);

        gridLayout_7->addWidget(checkBox_2, 3, 4, 1, 1);

        label_25 = new QLabel(layoutWidget2);
        label_25->setObjectName(QStringLiteral("label_25"));

        gridLayout_7->addWidget(label_25, 1, 0, 1, 1);

        label_26 = new QLabel(layoutWidget2);
        label_26->setObjectName(QStringLiteral("label_26"));

        gridLayout_7->addWidget(label_26, 2, 0, 2, 2);

        lineEdit_32 = new QLineEdit(layoutWidget2);
        lineEdit_32->setObjectName(QStringLiteral("lineEdit_32"));

        gridLayout_7->addWidget(lineEdit_32, 5, 3, 1, 1);

        lineEdit_28 = new QLineEdit(layoutWidget2);
        lineEdit_28->setObjectName(QStringLiteral("lineEdit_28"));

        gridLayout_7->addWidget(lineEdit_28, 1, 3, 1, 1);

        lineEdit_29 = new QLineEdit(layoutWidget2);
        lineEdit_29->setObjectName(QStringLiteral("lineEdit_29"));

        gridLayout_7->addWidget(lineEdit_29, 3, 1, 1, 1);

        lineEdit_27 = new QLineEdit(layoutWidget2);
        lineEdit_27->setObjectName(QStringLiteral("lineEdit_27"));

        gridLayout_7->addWidget(lineEdit_27, 1, 1, 1, 1);

        label_27 = new QLabel(layoutWidget2);
        label_27->setObjectName(QStringLiteral("label_27"));

        gridLayout_7->addWidget(label_27, 4, 0, 2, 2);

        lineEdit_31 = new QLineEdit(layoutWidget2);
        lineEdit_31->setObjectName(QStringLiteral("lineEdit_31"));

        gridLayout_7->addWidget(lineEdit_31, 5, 1, 1, 1);

        checkBox_3 = new QCheckBox(layoutWidget2);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setChecked(true);

        gridLayout_7->addWidget(checkBox_3, 5, 4, 1, 1);

        checkBox_4 = new QCheckBox(layoutWidget2);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setChecked(true);

        gridLayout_7->addWidget(checkBox_4, 1, 2, 1, 1);

        checkBox_5 = new QCheckBox(layoutWidget2);
        checkBox_5->setObjectName(QStringLiteral("checkBox_5"));
        checkBox_5->setChecked(true);

        gridLayout_7->addWidget(checkBox_5, 3, 2, 1, 1);

        checkBox_6 = new QCheckBox(layoutWidget2);
        checkBox_6->setObjectName(QStringLiteral("checkBox_6"));
        checkBox_6->setChecked(true);

        gridLayout_7->addWidget(checkBox_6, 5, 2, 1, 1);

        checkBox = new QCheckBox(layoutWidget2);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setChecked(true);

        gridLayout_7->addWidget(checkBox, 1, 4, 1, 1);

        layoutWidget3 = new QWidget(page_3);
        layoutWidget3->setObjectName(QStringLiteral("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(0, 10, 291, 136));
        gridLayout_6 = new QGridLayout(layoutWidget3);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        gridLayout_6->setHorizontalSpacing(15);
        gridLayout_6->setContentsMargins(10, 0, 10, 0);
        verticalComboBox = new QComboBox(layoutWidget3);
        verticalComboBox->setObjectName(QStringLiteral("verticalComboBox"));

        gridLayout_6->addWidget(verticalComboBox, 0, 1, 1, 1);

        lineEdit_9 = new QLineEdit(layoutWidget3);
        lineEdit_9->setObjectName(QStringLiteral("lineEdit_9"));

        gridLayout_6->addWidget(lineEdit_9, 0, 2, 1, 1);

        label_11 = new QLabel(layoutWidget3);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_6->addWidget(label_11, 4, 0, 1, 1);

        label_10 = new QLabel(layoutWidget3);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_6->addWidget(label_10, 3, 0, 1, 1);

        lineEdit_10 = new QLineEdit(layoutWidget3);
        lineEdit_10->setObjectName(QStringLiteral("lineEdit_10"));

        gridLayout_6->addWidget(lineEdit_10, 1, 2, 1, 1);

        lineEdit_13 = new QLineEdit(layoutWidget3);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));

        gridLayout_6->addWidget(lineEdit_13, 4, 1, 1, 2);

        lineEdit_12 = new QLineEdit(layoutWidget3);
        lineEdit_12->setObjectName(QStringLiteral("lineEdit_12"));

        gridLayout_6->addWidget(lineEdit_12, 3, 1, 1, 2);

        label_9 = new QLabel(layoutWidget3);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_6->addWidget(label_9, 2, 0, 1, 1);

        checkBox_7 = new QCheckBox(layoutWidget3);
        checkBox_7->setObjectName(QStringLiteral("checkBox_7"));
        checkBox_7->setChecked(true);

        gridLayout_6->addWidget(checkBox_7, 0, 3, 1, 1);

        lineEdit_11 = new QLineEdit(layoutWidget3);
        lineEdit_11->setObjectName(QStringLiteral("lineEdit_11"));

        gridLayout_6->addWidget(lineEdit_11, 2, 2, 1, 1);

        label_8 = new QLabel(layoutWidget3);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_6->addWidget(label_8, 1, 0, 1, 1);

        rotationComboBox = new QComboBox(layoutWidget3);
        rotationComboBox->setObjectName(QStringLiteral("rotationComboBox"));

        gridLayout_6->addWidget(rotationComboBox, 1, 1, 1, 1);

        label_7 = new QLabel(layoutWidget3);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_6->addWidget(label_7, 0, 0, 1, 1);

        horizontalComboBox = new QComboBox(layoutWidget3);
        horizontalComboBox->setObjectName(QStringLiteral("horizontalComboBox"));

        gridLayout_6->addWidget(horizontalComboBox, 2, 1, 1, 1);

        checkBox_8 = new QCheckBox(layoutWidget3);
        checkBox_8->setObjectName(QStringLiteral("checkBox_8"));
        checkBox_8->setChecked(true);

        gridLayout_6->addWidget(checkBox_8, 1, 3, 1, 1);

        checkBox_9 = new QCheckBox(layoutWidget3);
        checkBox_9->setObjectName(QStringLiteral("checkBox_9"));
        checkBox_9->setChecked(true);

        gridLayout_6->addWidget(checkBox_9, 2, 3, 1, 1);

        checkBox_10 = new QCheckBox(layoutWidget3);
        checkBox_10->setObjectName(QStringLiteral("checkBox_10"));
        checkBox_10->setChecked(true);

        gridLayout_6->addWidget(checkBox_10, 3, 3, 1, 1);

        checkBox_11 = new QCheckBox(layoutWidget3);
        checkBox_11->setObjectName(QStringLiteral("checkBox_11"));
        checkBox_11->setChecked(true);

        gridLayout_6->addWidget(checkBox_11, 4, 3, 1, 1);

        toolBox->addItem(page_3, QStringLiteral("Position"));
        pushButton = new QPushButton(tab);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(110, 430, 80, 22));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        tabWidget->addTab(tab_2, QString());
        dockWidget->setWidget(dockWidgetContents_3);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), dockWidget);

        menubar->addAction(menuDosya->menuAction());
        menubar->addAction(menuModifiye->menuAction());
        menubar->addAction(menuView->menuAction());
        menubar->addAction(menuAdd_Object->menuAction());
        menubar->addAction(menuAyarlar->menuAction());
        menuDosya->addAction(actionKaydet);
        menuDosya->addAction(actionSaveAs);
        menuDosya->addSeparator();
        menuDosya->addAction(action_k);
        menuAyarlar->addAction(menuG_r_nt->menuAction());
        menuG_r_nt->addAction(actionKat);
        menuG_r_nt->addAction(actionWireframe);
        menuG_r_nt->addAction(actionGizli_izgi);
        menuModifiye->addAction(actionKopyala);
        menuModifiye->addAction(actionMove_Object);
        menuModifiye->addAction(actionDeleteObjects);
        menuAdd_Object->addAction(actionAdd_axis);
        menuAdd_Object->addAction(actionDraw_Column);
        menuAdd_Object->addAction(actionDraw_beam);
        menuView->addAction(actionFront_View);
        menuView->addAction(actionBack_View);
        menuView->addAction(actionTop_View);
        menuView->addAction(actionBottom_View);
        menuView->addAction(actionLeft_View);
        menuView->addAction(actionRight_View);
        menuView->addAction(action3D_View);
        menuView->addSeparator();
        menuView->addAction(actionViewToWorkplane);
        menuView->addSeparator();
        menuView->addAction(menuViewStyle->menuAction());
        menuView->addAction(menuWorkPlane->menuAction());
        menuViewStyle->addAction(actionKat);
        menuViewStyle->addAction(actionWireframe);
        menuWorkPlane->addAction(actionByTwoPoints);
        toolBar->addAction(actionDraw_Column);
        toolBar->addAction(actionDraw_beam);
        toolBar->addAction(actionDENEME);
        toolBar_2->addAction(actionKopyala);
        toolBar_2->addAction(actionMove_Object);
        toolBar_2->addAction(actionRotate_Object);
        toolBar_3->addAction(action3D_View);
        toolBar_3->addAction(actionFront_View);
        toolBar_3->addAction(actionBack_View);
        toolBar_3->addAction(actionTop_View);
        toolBar_3->addAction(actionBottom_View);
        toolBar_3->addAction(actionLeft_View);
        toolBar_3->addAction(actionRight_View);
        toolBar_3->addAction(actionFit_All);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);
        toolBox->setCurrentIndex(2);
        verticalComboBox->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "tsDesign", Q_NULLPTR));
        actionLine->setText(QApplication::translate("MainWindow", "Line", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionLine->setToolTip(QApplication::translate("MainWindow", "Draw a line...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionFront_View->setText(QApplication::translate("MainWindow", "Front View", Q_NULLPTR));
        actionBack_View->setText(QApplication::translate("MainWindow", "Back View", Q_NULLPTR));
        actionTop_View->setText(QApplication::translate("MainWindow", "Top View", Q_NULLPTR));
        actionBottom_View->setText(QApplication::translate("MainWindow", "Bottom View", Q_NULLPTR));
        actionLeft_View->setText(QApplication::translate("MainWindow", "Left View", Q_NULLPTR));
        actionRight_View->setText(QApplication::translate("MainWindow", "Right View", Q_NULLPTR));
        action3D_View->setText(QApplication::translate("MainWindow", "3D View", Q_NULLPTR));
        actionFit_All->setText(QApplication::translate("MainWindow", "Fit All", Q_NULLPTR));
        actionRotate_Object->setText(QApplication::translate("MainWindow", "Rotate Object", Q_NULLPTR));
        actionGrid->setText(QApplication::translate("MainWindow", "Grid", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionGrid->setToolTip(QApplication::translate("MainWindow", "Show/Hide Grid", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionWork_Plane->setText(QApplication::translate("MainWindow", "Work Plane", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionWork_Plane->setToolTip(QApplication::translate("MainWindow", "Choose Workplane", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionKat->setText(QApplication::translate("MainWindow", "Kat\304\261 Model", Q_NULLPTR));
        actionWireframe->setText(QApplication::translate("MainWindow", "Tel \303\207er\303\247eve", Q_NULLPTR));
        actionGizli_izgi->setText(QApplication::translate("MainWindow", "Gizli \303\207izgi", Q_NULLPTR));
        action_k->setText(QApplication::translate("MainWindow", "Quit", Q_NULLPTR));
        actionKopyala->setText(QApplication::translate("MainWindow", "Copy", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionKopyala->setToolTip(QApplication::translate("MainWindow", "Copy object", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionKaydet->setText(QApplication::translate("MainWindow", "Save", Q_NULLPTR));
        actionMove_Object->setText(QApplication::translate("MainWindow", "Move Object", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionMove_Object->setToolTip(QApplication::translate("MainWindow", "Move object", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionDraw_Column->setText(QApplication::translate("MainWindow", "Draw Column", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionDraw_Column->setToolTip(QApplication::translate("MainWindow", "Draw column", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionDraw_beam->setText(QApplication::translate("MainWindow", "Draw beam", Q_NULLPTR));
        actionDENEME->setText(QApplication::translate("MainWindow", "DENEME", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionDENEME->setToolTip(QApplication::translate("MainWindow", "Deneme butonu", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionAdd_axis->setText(QApplication::translate("MainWindow", "Add axis", Q_NULLPTR));
        actionSaveAs->setText(QApplication::translate("MainWindow", "Save as...", Q_NULLPTR));
        actionMove->setText(QApplication::translate("MainWindow", "Move", Q_NULLPTR));
        actionDeleteObjects->setText(QApplication::translate("MainWindow", "Delete objects", Q_NULLPTR));
        actionByTwoPoints->setText(QApplication::translate("MainWindow", "by two points", Q_NULLPTR));
        actionViewToWorkplane->setText(QApplication::translate("MainWindow", "View to workplane", Q_NULLPTR));
        menuDosya->setTitle(QApplication::translate("MainWindow", "File", Q_NULLPTR));
        menuAyarlar->setTitle(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
        menuG_r_nt->setTitle(QApplication::translate("MainWindow", "G\303\266r\303\274nt\303\274", Q_NULLPTR));
        menuModifiye->setTitle(QApplication::translate("MainWindow", "Edit", Q_NULLPTR));
        menuAdd_Object->setTitle(QApplication::translate("MainWindow", "Add Object", Q_NULLPTR));
        menuView->setTitle(QApplication::translate("MainWindow", "View", Q_NULLPTR));
        menuViewStyle->setTitle(QApplication::translate("MainWindow", "View style", Q_NULLPTR));
        menuWorkPlane->setTitle(QApplication::translate("MainWindow", "Work Plane", Q_NULLPTR));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", Q_NULLPTR));
        toolBar_2->setWindowTitle(QApplication::translate("MainWindow", "toolBar_2", Q_NULLPTR));
        toolBar_3->setWindowTitle(QApplication::translate("MainWindow", "toolBar_3", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Class", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Material", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Profile", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Name", Q_NULLPTR));
        toolButton->setText(QApplication::translate("MainWindow", "...", Q_NULLPTR));
        toolButton_2->setText(QApplication::translate("MainWindow", "...", Q_NULLPTR));
        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("MainWindow", "General", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Part Numbering", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "Assembly Numbering", Q_NULLPTR));
        toolBox->setItemText(toolBox->indexOf(page_2), QApplication::translate("MainWindow", "Numbering Series", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindow", "End Offsets", Q_NULLPTR));
        label_24->setText(QApplication::translate("MainWindow", "End", Q_NULLPTR));
        label_23->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
        checkBox_2->setText(QString());
        label_25->setText(QApplication::translate("MainWindow", "Dx", Q_NULLPTR));
        label_26->setText(QApplication::translate("MainWindow", "Dy", Q_NULLPTR));
        label_27->setText(QApplication::translate("MainWindow", "Dz", Q_NULLPTR));
        checkBox_3->setText(QString());
        checkBox_4->setText(QString());
        checkBox_5->setText(QString());
        checkBox_6->setText(QString());
        checkBox->setText(QString());
        lineEdit_9->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        label_11->setText(QApplication::translate("MainWindow", "Bottom", Q_NULLPTR));
        label_10->setText(QApplication::translate("MainWindow", "Top", Q_NULLPTR));
        lineEdit_10->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_13->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_12->setText(QApplication::translate("MainWindow", "7000", Q_NULLPTR));
        label_9->setText(QApplication::translate("MainWindow", "Horizontal", Q_NULLPTR));
        checkBox_7->setText(QString());
        lineEdit_11->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "Rotation", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "Vertical", Q_NULLPTR));
        checkBox_8->setText(QString());
        checkBox_9->setText(QString());
        checkBox_10->setText(QString());
        checkBox_11->setText(QString());
        toolBox->setItemText(toolBox->indexOf(page_3), QApplication::translate("MainWindow", "Position", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MainWindow", "Modify", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Properties", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Connections", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

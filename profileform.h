#ifndef PROFILEFORM_H
#define PROFILEFORM_H

#include <QWidget>

namespace Ui {
class profileForm;
}

class profileForm : public QWidget
{
    Q_OBJECT

public:
    explicit profileForm(QWidget *parent = nullptr);
    ~profileForm();

private:
    Ui::profileForm *ui;
};

#endif // PROFILEFORM_H

#include "snapmanager.h"

SnapManager::SnapManager(const Handle(AIS_InteractiveContext) Context,Handle(V3d_View) View)
{
    theContext = Context;
    theView = View;
}

void SnapManager::receiveMouseMove(const int theFlags, const QPoint thePoint)
{
    Graphic3d_Vec2i V;

    V.x() = thePoint.x();
    V.y() = thePoint.y();

    VK->PickPoint(realMouseCoordinate,theContext,theView,V,true);
    theContext->MainSelector()->Pick(thePoint.x(),thePoint.y(),theView);
    realMouseCoordinate =   theconvertToPlane(theView,thePoint.x(),thePoint.y());

    const gp_Pnt pos3d =  theContext->MainSelector()->NbPicked() > 0 ? theContext->MainSelector()->PickedPoint(1) : realMouseCoordinate;
    DetectSnap(pos3d);
}

void SnapManager::receiveMouseUp(const int theFlags, const QPoint thePoint)
{
    //
}

bool SnapManager::DetectSnap(gp_Pnt dpoint)
{

    Handle_AIS_InteractiveObject hObj = theContext->DetectedInteractive();

    if(!hObj.IsNull())
    {
        //Access the highlighted object
        Handle(AIS_Shape) hShape = Handle(AIS_Shape)::DownCast(hObj);
        if(hShape.IsNull()) //not AIS_Shape
        {
            return false;
        }

        TopoDS_Shape  shape = hShape->Shape(); //Get TopoDS_Shape (geometry)

        if(shape.ShapeType()== TopAbs_SOLID )
        {
            TopoDS_Shape sShape =  hShape->Shape();
            snappedPoint = findMidPointOnObjectsNearestEdge(theView,sShape,dpoint,snapStatus);
            if (snapStatus) theSnapStatus = snapMode_Mid;
        }

    }
}

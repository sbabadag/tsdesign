#include "copyElementsDialog.h"
#include "ui_copyElementsDialog.h"

copy_elements_dialog::copy_elements_dialog(QWidget *parent) :
                                                              QWidget(parent),
                                                              ui(new Ui::copy_elements_dialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Window | Qt::WindowStaysOnTopHint | Qt::WindowCloseButtonHint);  //Qt::FramelessWindowHint |
}

copy_elements_dialog::~copy_elements_dialog()
{
    delete ui;
}

void copy_elements_dialog::firePickPointSignal()
{
    emit sendPickPointRequest();
}

void copy_elements_dialog::setPickedPoint(gp_Pnt P)
{
    pickedPoint = P;
    ui->lineEdit->setText(QString("%1").arg(pickedPoint.X()));
    ui->lineEdit_2->setText(QString("%1").arg(pickedPoint.Y()));
    ui->lineEdit_3->setText(QString("%1").arg(pickedPoint.Z()));
}

void copy_elements_dialog::on_pushButton_2_clicked()
{
    close();
}

void copy_elements_dialog::on_pushButton_clicked()
{
    // do the copy operation
    double x,y,z;
    int no;
    x = ui->lineEdit->text().toDouble();
    y = ui->lineEdit_2->text().toDouble();
    z = ui->lineEdit_3->text().toDouble();
    no = ui->lineEdit_4->text().toInt();

    copyParameters.SetX(x);
    copyParameters.SetY(y);
    copyParameters.SetZ(z);

    no_o = no;


    fireCopyRequestSignal();

}

void copy_elements_dialog::on_toolButton_triggered(QAction *arg1)
{
    //
}

void copy_elements_dialog::on_toolButton_clicked()
{
    firePickPointSignal();
}

void copy_elements_dialog::on_toolButton_2_clicked()
{
    ui->lineEdit->setText("0");
    ui->lineEdit_2->setText("0");
    ui->lineEdit_3->setText("0");
    ui->lineEdit_4->setText("1");
}

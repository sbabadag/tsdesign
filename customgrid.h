#ifndef CUSTOMGRID_H
#define CUSTOMGRID_H

#include <AIS_InteractiveContext.hxx>
#include <QString>
#include <QVector>
#include <Prs3d_LineAspect.hxx>
#include <AIS_Shape.hxx>
#include <TopoDS_Shape.hxx>
#include <TopTools_ListOfShape.hxx>

enum drawPlaneType {
    drawPlaneXOY,
    drawPlaneXOZ,
    drawPlaneYOZ
};

enum gridElementType {
    gridElementX,
    gridElementY,
    gridElementZ
};

struct CGrid
{
    CGrid(QString label,gp_Pnt s,gp_Pnt e)
    {
        Label = label;
        sPt = s;
        ePt = e;
    }
    QString Label;
    gp_Pnt sPt,ePt;

};

class customgrid
{
public:
    customgrid(Handle(AIS_InteractiveContext) Context,QString xGridValues,
                                                      QString yGridValues,
                                                      QString zGridValues,
                                                      QString xGridLabels,
                                                      QString yGridLabels,
                                                      QString zGridLabels);
    ~customgrid() {}
    void draw();
    void hide();
    void deleteAll();
    void setOrigin(gp_Pnt origin = gp_Pnt(0,0,0));
    void setGridPlane(drawPlaneType plane) {planeToDraw = plane;draw();}
    void setColor(Quantity_NameOfColor aColor) {color=aColor;draw();}
    void prepare();
    void setGrid(gp_Pnt aOrigin,
                 QString xGridValues,
                 QString yGridValues,
                 QString zGridValues,
                 QString xGridLabels,
                 QString yGridLabels,
                 QString zGridLabels);

    QVector<gp_Pnt*> getGridPoints() {return gridPoints;}
private:
    Handle(AIS_InteractiveContext) theContext;
    Handle(AIS_Shape) XOY_objects;
    Handle(AIS_Shape) XOZ_objects;
    Handle(AIS_Shape) YOZ_objects;
    Handle(Prs3d_LineAspect) lineProps;
    Quantity_NameOfColor color = Quantity_NOC_GRAY50 ;
    QVector<CGrid*> xLines;
    QVector<CGrid*> yLines;
    QVector<CGrid*> zLines;
    QVector<Standard_Real> levels;
    Aspect_TypeOfLine lineType = Aspect_TOL_DASH ;
    Standard_Real lineThickness = 1;
    drawPlaneType planeToDraw;
    Standard_Real extraDistance = 1000;
    QVector<gp_Pnt*> gridPoints;
    gp_Pnt origin;



//
    void createObjects();


};

#endif // CUSTOMGRID_H


/*
 * myDrawer = new AIS_Drawer();
myDrawer->SetLineAspect(Prs3d_LineAspect(...));
myInteractiveObject->SetAttributes( myDrawer );
*/

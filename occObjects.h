#ifndef QOCC_OBJECT_H
#define QOCC_OBJECT_H

#include <qstring.h>
#include <AIS_InteractiveContext.hxx>
#include <AIS_Shape.hxx>
#include <gp_Pnt.hxx>
#include <Quantity_Color.hxx>
#include <TopoDS_Vertex.hxx>
#include <Quantity_Color.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <Prs3d_PointAspect.hxx>
#include <AIS_Shape.hxx>
#include <TopoDS_Shape.hxx>
#include <QObject>
#include "occSnippets.h"




class QOCC_Object : public QObject
{
    Q_OBJECT

public:
    QOCC_Object(const Handle(AIS_InteractiveContext) &aContext,bool updateViewer)
       {
        Context = aContext;
        myUpdateViewer = updateViewer;
       }
    virtual void draw()
    {
        Context->Redisplay(shape,true);
    }
    virtual void setColor(Quantity_NameOfColor Color) {color = Color;shape->SetColor(color);}
    virtual void setOrientation(float deg) {orientation=deg;}
    virtual Standard_Real getOrientation() {return orientation;}
    virtual void setOffset(gp_Pnt2d offset);
    //
    int objType;
    virtual Handle(AIS_Shape) getAISShape() {return shape;}



private:

protected:
    Handle(AIS_InteractiveContext) Context ;
    Handle(AIS_Shape) shape;
    Quantity_NameOfColor color;
    Standard_Real orientation = 0;
    gp_Pnt2d offset = gp_Pnt2d(0,0);
    bool myUpdateViewer;
    gp_Ax3 profileAxis;



};


class QOCC_Point : public QOCC_Object
{

public:
    QOCC_Point(Handle(AIS_InteractiveContext) &aContext,gp_Pnt P,bool updateViewer) : QOCC_Object(aContext,updateViewer)
    {
        objType = 0;
        color = Quantity_NOC_BLUE1;
        p = P;
        TopoDS_Vertex V1 = BRepBuilderAPI_MakeVertex( p );
        shape = new AIS_Shape(V1);
        Handle_Prs3d_PointAspect myPointAspect=new Prs3d_PointAspect(Aspect_TOM_O,color,0.1);
        shape->Attributes()->SetPointAspect(myPointAspect);

    }
    virtual void draw() {QOCC_Object::draw();}
    virtual void setColor(Quantity_NameOfColor Color) {QOCC_Object::setColor(Color);}
private:
    gp_Pnt p;
};

class QOCC_ipe_Profile : public QOCC_Object
{
public:
    QOCC_ipe_Profile(Handle(AIS_InteractiveContext) &aContext,QString profile,gp_Pnt P1,gp_Pnt P2,bool updateViewer) : QOCC_Object(aContext,updateViewer)
    {
        objType = 100;
        p1 = P1;
        p2 = P2;
        color = Quantity_NOC_BLUE1;
        profileName= profile;
        profileAxis = gp_Ax3(p1, gp_Dir(gp_Vec(p1,p2)));
   //     profileAxis.SetXDirection(Context->CurrentViewer()->PrivilegedPlane().XDirection());
        createShape();

    }
    virtual void draw() {QOCC_Object::draw();}
    virtual void setOrientation(float deg);
    virtual void setColor(Quantity_NameOfColor Color) {QOCC_Object::setColor(Color);}
    virtual void setOffset(gp_Pnt2d offset);

    QString getProfileName() {return profileName;}
    void getPoints(gp_Pnt &P1,gp_Pnt &P2) {P1=p1;P2=p2;}


private:
    gp_Pnt p1,p2;
    void createShape();
    QString profileName;
};


#endif // QOCC_OBJECT_H

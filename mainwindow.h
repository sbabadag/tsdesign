#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "occObjects.h"
#include "copyElementsDialog.h"
#include "axisform.h"
#include <QMenu>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QOCC_ipe_Profile *Prf;
    axisForm* axis_dialog;


private slots:
    void on_actionLine_triggered();
    void on_actionFront_View_triggered();
    void on_actionFit_All_triggered();
    void on_actionBack_View_triggered();
    void on_actionTop_View_triggered();
    void on_actionBottom_View_triggered();
    void on_actionLeft_View_triggered();
    void on_actionRight_View_triggered();
    void on_action3D_View_triggered();
    void on_actionRotate_Object_triggered();
    void on_actionGrid_triggered();
    void getReceivedMouseCoordinates(gp_Pnt &p);
    void getSetButtonStatus(int button, bool status);
    void getAddNewGrid(gp_Pnt aOrigin,QString xGridValues,QString yGridValues,QString zGridValues,QString xGridLabels,QString yGridLabels,QString zGridLabels);
    void on_actionWork_Plane_triggered();
    void on_actionKat_triggered();
    void on_actionWireframe_triggered();
    void on_actionGizli_izgi_triggered();
    void on_action_k_triggered();
    void on_actionKopyala_triggered();
    void on_actionKaydet_triggered();
    void on_actionDraw_Column_triggered();
    void on_actionDraw_beam_triggered();
    void on_actionDENEME_triggered();
    void on_actionAdd_axis_triggered();
    void on_actionDeleteObjects_triggered();
    void on_actionByTwoPoints_triggered();
    void on_actionViewToWorkplane_triggered();

protected:
    virtual void keyPressEvent(QKeyEvent* event) ;



private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H

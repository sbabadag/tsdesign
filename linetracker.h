#ifndef LINETRACKER_H
#define LINETRACKER_H

#include <QObject>
#include <AIS_Line.hxx>
#include <Geom_Point.hxx>
#include <AIS_InteractiveContext.hxx>

class lineTracker
{

public:
    lineTracker(Handle(AIS_InteractiveContext) Context,Handle(V3d_View) aView);

    void setStart(gp_Pnt P);
    void setEnd(gp_Pnt P);
    void startTracking(gp_Pnt P);
    void finishTracking();
    bool active = false;

private:
    void draw();
    void hide();
    Handle(AIS_InteractiveContext)  theContext;
    Handle(V3d_View)                theView;
    Handle(AIS_Line) tracker;
    gp_Pnt startPoint;
    gp_Pnt endPoint;

};

#endif // LINETRACKER_H

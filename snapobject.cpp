#include "snapobject.h"
#include "occSnippets.h"
#include <Graphic3d_Vec2.hxx>



void snapObject::mouseMoveEvent(QMouseEvent *theEvent)
{
    if (!active) return;
    RealCoordinate = getWorldPoint(theEvent->pos());
    gp_Pnt onShapePoint(0,0,0);


    detectObject(theEvent->pos());


        theContext->MainSelector()->Pick(theEvent->pos().x(),theEvent->pos().y(),theView);



        onShapePoint = theContext->MainSelector()->NbPicked() > 0 ? theContext->MainSelector()->PickedPoint(1) : RealCoordinate;

        if (theContext->MainSelector()->NbPicked() > 0)
        {
            if (!selectedShape.IsNull())
          snapStatus = getShapeSnap(onShapePoint);
        }
          else
        {
           snapStatus = getGridSnap(snappedPoint);
        }

}

bool snapObject::getGridSnap(gp_Pnt& P)
{
    foreach (gp_Pnt* p,gridPoints ) {
        if (p->Distance(RealCoordinate) < snapTolerance)
        {
            P = *p;
            snapStatus = true;
            setColor(Quantity_Color(Quantity_NOC_BLUE1));
            draw();
            return snapStatus;
        }
    }
    hide();
    return false;
}

bool snapObject::getShapeSnap(gp_Pnt& P)
{

    static Standard_Real dist=1;

    if (mySnapMode & snapMode_End)
    {
        snappedPoint = findEndPointOnEdge(getSelectedEdge(dist),P,dist/3.0,snapStatus);
        if (snapStatus) setColor(Quantity_Color(Quantity_NOC_YELLOW1));
    }

    if (!snapStatus)
    {
    if (mySnapMode & snapMode_Mid)
        snappedPoint = findMidPointOnEdge(getSelectedEdge(dist),P,dist/3.0,snapStatus);
    if (snapStatus) setColor(Quantity_Color(Quantity_NOC_CYAN1));

    }

    if (snapStatus) draw();else hide();
    return snapStatus;
}

void snapObject::draw()
{
    hide();
    gp_Trsf trsf;
    trsf.SetValues (1, 0, 0, snappedPoint.X(), 0, 1, 0, snappedPoint.Y(),0, 0, 1, snappedPoint.Z());
    theContext->SetLocation(theSnapObject,trsf);

        theContext->Display(theSnapObject,AIS_Shaded,-1,true,false);
        hidden = false;
}

gp_Pnt snapObject::getWorldPoint(QPoint QP)
{
    return theconvertToPlane(theView,QP.x(),QP.y());
}

void snapObject::hide()
{
  theContext->Remove(theSnapObject,true);
  hidden = true;
}

void snapObject::detectObject(QPoint P)
{
    theContext->Deactivate();
    theContext->Activate (AIS_Shape::SelectionMode (TopAbs_EDGE),Standard_False);
    theContext->SetAutomaticHilight(false);


    theContext->MoveTo(P.x(), P.y(), theView,true);


    if(theContext->HasDetected())
    {
        theContext->InitDetected();

        do
        {
            TopoDS_Shape shape = theContext->DetectedShape();

            if(!shape.IsNull())
            {
                switch (shape.ShapeType()) {
                case TopAbs_VERTEX:{
                                    gp_Pnt P;
                                    const TopoDS_Vertex & vertex = TopoDS::Vertex(shape);
                                    P = BRep_Tool::Pnt(vertex);
                }
                        break;
                case TopAbs_WIRE:

                    break;
                case TopAbs_EDGE:{
                                  TopoDS_Edge sShape =  TopoDS::Edge(shape);
                                  selectedEdge = sShape;
                }
                    break;
                default:
                    break;
                }
            }

            theContext->NextDetected();
        }
        while(theContext->MoreDetected());
    }

}

TopoDS_Edge snapObject::getSelectedEdge(Standard_Real &length)
{

    // Take the first and the last vertices from edge
    TopoDS_Vertex aVFirst = TopExp::FirstVertex(selectedEdge);
    TopoDS_Vertex aVLast  = TopExp::LastVertex(selectedEdge);

    // Take geometrical information from vertices.
    gp_Pnt pnt1 = BRep_Tool::Pnt(aVFirst);
    gp_Pnt pnt2 = BRep_Tool::Pnt(aVLast);

    if (pnt1.Distance(pnt2) > 500) length = 20; else length = pnt1.Distance(pnt2);

    return selectedEdge;
}

void snapObject::setColor(Quantity_Color aColor)
{
    Handle_Prs3d_PointAspect myPointAspect = new Prs3d_PointAspect(Aspect_TOM_O_PLUS ,aColor,snapObjectSize);
    theSnapObject->Attributes()->SetPointAspect(myPointAspect);
}

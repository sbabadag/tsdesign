#include "linetracker.h"
#include <Geom_CartesianPoint.hxx>

lineTracker::lineTracker(Handle(AIS_InteractiveContext) Context,Handle(V3d_View) aView)
{
    theContext = Context;
    theView = aView;

    Handle(Geom_Point) pnta = new Geom_CartesianPoint(startPoint);
    Handle(Geom_Point) pntb = new Geom_CartesianPoint(endPoint);
    tracker = new AIS_Line(pnta,pntb);

    tracker->SetColor(Quantity_Color(Quantity_NOC_CYAN1));
    tracker->SetWidth(2);


    startPoint = gp_Pnt(0,0,0);
    endPoint = gp_Pnt(0,0,1);
}

void lineTracker::draw()
{
    if (active) theContext->Display(tracker,0,-1,true,true);
}

void lineTracker::hide()
{
    theContext->Remove(tracker,true);
}

void lineTracker::setStart(gp_Pnt P)
{
    startPoint = P;

    Handle(Geom_Point) pnta = new Geom_CartesianPoint(startPoint);
    Handle(Geom_Point) pntb = new Geom_CartesianPoint(endPoint);

    tracker->SetPoints(pnta,pntb);

    hide();
    draw();
}

void lineTracker::setEnd(gp_Pnt P)
{
    endPoint = P;

    Handle(Geom_Point) pnta = new Geom_CartesianPoint(startPoint);
    Handle(Geom_Point) pntb = new Geom_CartesianPoint(endPoint);

    tracker->SetPoints(pnta,pntb);

    hide();
    draw();
}

void lineTracker::startTracking(gp_Pnt P)
{
    active = true;
    startPoint = P;
}

void lineTracker::finishTracking()
{
    active = false;
    hide();
}


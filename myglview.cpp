﻿#include "myglview.h"
#include "mainwindow.h"
#include <GeomAPI_ProjectPointOnCurve.hxx>
#include <Geom_Curve.hxx>
#include <Geom_OffsetCurve.hxx>
#include <OpenGl_GraphicDriver.hxx>
#include <QDebug>
#include <QElapsedTimer>
#include <QMainWindow>
#include <QProgressDialog>
#include <QStatusBar>
#include <gp_Trsf.hxx>
//
#include "customgrid.h"
#include <BRepPrimAPI_MakeBox.hxx>
//
#include <HLRBRep_Algo.hxx>
#include <HLRBRep_HLRToShape.hxx>

#include <QMenu>
#include <QMouseEvent>
#include <QRubberBand>
#include <QStyleFactory>

#include <AIS_InteractiveContext.hxx>
#include <AIS_Point.hxx>
#include <AIS_Shape.hxx>
#include <Aspect_DisplayConnection.hxx>
#include <Aspect_Handle.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <BRepAdaptor_Surface.hxx>
#include <BRepAlgo.hxx>
#include <BRepAlgoAPI_Algo.hxx>
#include <BRepAlgoAPI_Common.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepAlgoAPI_Section.hxx>
#include <BRepAlgo_Common.hxx>
#include <BRepBuilderAPI_Copy.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRepFeat_SplitShape.hxx>
#include <BRepFilletAPI_MakeFillet.hxx>
#include <BRepLib.hxx>
#include <BRepOffsetAPI_MakePipe.hxx>
#include <BRepOffsetAPI_MakeThickSolid.hxx>
#include <BRepOffsetAPI_ThruSections.hxx>
#include <BRepPrimAPI_MakeCone.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>
#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeTorus.hxx>
#include <BRepTools.hxx>
#include <BRep_Tool.hxx>
#include <DsgPrs_LengthPresentation.hxx>
#include <ElSLib.hxx>
#include <GCE2d_MakeSegment.hxx>
#include <GCPnts_AbscissaPoint.hxx>
#include <GCPnts_TangentialDeflection.hxx>
#include <GC_MakeArcOfCircle.hxx>
#include <GC_MakeCircle.hxx>
#include <GC_MakeLine.hxx>
#include <GC_MakeSegment.hxx>
#include <Geom2d_Ellipse.hxx>
#include <Geom2d_TrimmedCurve.hxx>
#include <GeomAPI_IntCS.hxx>
#include <GeomAPI_Interpolate.hxx>
#include <GeomAbs_CurveType.hxx>
#include <GeomAdaptor_Curve.hxx>
#include <GeomConvert_CompCurveToBSplineCurve.hxx>
#include <GeomLib.hxx>
#include <GeomToIGES_GeomSurface.hxx>
#include <GeomTools_Curve2dSet.hxx>
#include <Geom_Axis2Placement.hxx>
#include <Geom_BSplineSurface.hxx>
#include <Geom_BezierCurve.hxx>
#include <Geom_BezierSurface.hxx>
#include <Geom_CartesianPoint.hxx>
#include <Geom_ConicalSurface.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <Geom_Line.hxx>
#include <Geom_OffsetSurface.hxx>
#include <Geom_Plane.hxx>
#include <Geom_RectangularTrimmedSurface.hxx>
#include <Geom_SphericalSurface.hxx>
#include <Geom_Surface.hxx>
#include <Geom_SurfaceOfLinearExtrusion.hxx>
#include <Geom_SurfaceOfRevolution.hxx>
#include <Geom_ToroidalSurface.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <Graphic3d_GraphicDriver.hxx>
#include <Graphic3d_NameOfMaterial.hxx>
#include <IGESControl_Controller.hxx>
#include <IGESControl_Writer.hxx>
#include <Interface_Static.hxx>
#include <MMgt_TShared.hxx>
#include <OSD_Environment.hxx>
#include <OpenGl_GraphicDriver.hxx>
#include <Precision.hxx>
#include <ProjLib.hxx>
#include <Prs3d_IsoAspect.hxx>
#include <Prs3d_LineAspect.hxx>
#include <Prs3d_PointAspect.hxx>
#include <Prs3d_Projector.hxx>
#include <Prs3d_Text.hxx>
#include <Quantity_Factor.hxx>
#include <Quantity_Length.hxx>
#include <Quantity_NameOfColor.hxx>
#include <Quantity_PhysicalQuantity.hxx>
#include <Quantity_PlaneAngle.hxx>
#include <Quantity_TypeOfColor.hxx>
#include <SelectMgr_EntityOwner.hxx>
#include <SelectMgr_Filter.hxx>
#include <SelectMgr_ListOfFilter.hxx>
#include <SelectMgr_SelectableObject.hxx>
#include <SelectMgr_Selection.hxx>
#include <SelectMgr_SelectionManager.hxx>
#include <Standard_Boolean.hxx>
#include <Standard_CString.hxx>
#include <Standard_DefineHandle.hxx>
#include <Standard_ErrorHandler.hxx>
#include <Standard_IStream.hxx>
#include <Standard_Integer.hxx>
#include <Standard_Macro.hxx>
#include <Standard_NotImplemented.hxx>
#include <Standard_OStream.hxx>
#include <Standard_Real.hxx>
#include <StdPrs_Curve.hxx>
#include <StdPrs_Point.hxx>
#include <StdPrs_PoleCurve.hxx>
#include <StdSelect_EdgeFilter.hxx>
#include <StdSelect_ShapeTypeFilter.hxx>
#include <TColStd_HSequenceOfTransient.hxx>
#include <TColStd_MapIteratorOfMapOfTransient.hxx>
#include <TColStd_MapOfTransient.hxx>
#include <TColgp_Array1OfPnt2d.hxx>
#include <TColgp_HArray1OfPnt.hxx>
#include <TColgp_HArray1OfPnt2d.hxx>
#include <TCollection_AsciiString.hxx>
#include <TopExp.hxx>
#include <TopExp_Explorer.hxx>
#include <TopTools_DataMapOfShapeInteger.hxx>
#include <TopTools_DataMapOfShapeReal.hxx>
#include <TopTools_HSequenceOfShape.hxx>
#include <TopTools_IndexedDataMapOfShapeAddress.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Compound.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Solid.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Wire.hxx>
#include <UnitsAPI.hxx>
#include <V3d_AmbientLight.hxx>
#include <V3d_DirectionalLight.hxx>
#include <V3d_PositionalLight.hxx>
#include <V3d_View.hxx>
#include <V3d_Viewer.hxx>
#include <gp.hxx>
#include <gp_Ax1.hxx>
#include <gp_Ax2.hxx>
#include <gp_Ax2d.hxx>
#include <gp_Dir.hxx>
#include <gp_Dir2d.hxx>
#include <gp_Pnt.hxx>
#include <gp_Pnt2d.hxx>
#include <gp_Trsf.hxx>
#include <gp_Vec.hxx>

#include <BRepAlgoAPI_Fuse.hxx>

#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_Transform.hxx>

#include <BRepFilletAPI_MakeFillet.hxx>

#include <BRepLib.hxx>

#include <BRepOffsetAPI_MakeThickSolid.hxx>
#include <BRepOffsetAPI_ThruSections.hxx>

#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakePrism.hxx>

#include <GC_MakeArcOfCircle.hxx>
#include <GC_MakeSegment.hxx>

#include <GCE2d_MakeSegment.hxx>

#include <gp.hxx>
#include <gp_Ax1.hxx>
#include <gp_Ax2.hxx>
#include <gp_Ax2d.hxx>
#include <gp_Dir.hxx>
#include <gp_Dir2d.hxx>
#include <gp_Pnt.hxx>
#include <gp_Pnt2d.hxx>
#include <gp_Trsf.hxx>
#include <gp_Vec.hxx>

#include <Geom_CylindricalSurface.hxx>
#include <Geom_Plane.hxx>
#include <Geom_Surface.hxx>
#include <Geom_TrimmedCurve.hxx>

#include <Geom2d_Ellipse.hxx>
#include <Geom2d_TrimmedCurve.hxx>

#include <TopExp_Explorer.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Compound.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Wire.hxx>

#include <Approx_CurveOnSurface.hxx>
#include <BRepAlgoAPI_Cut.hxx>
#include <BRepBuilderAPI_Copy.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_TransitionMode.hxx>
#include <BRepClass_FaceClassifier.hxx>
#include <BRepGProp.hxx>
#include <BRepOffsetAPI_MakePipe.hxx>
#include <BRepOffsetAPI_MakePipeShell.hxx>
#include <GProp_GProps.hxx>
#include <Geom2dAdaptor_HCurve.hxx>
#include <Geom2d_BezierCurve.hxx>
#include <GeomAdaptor_HSurface.hxx>
#include <GeomFill_Pipe.hxx>
#include <GeomToStep_MakeCartesianPoint.hxx>
#include <GeomToStep_MakePolyline.hxx>
#include <Geom_BSplineCurve.hxx>
#include <Geom_BezierCurve.hxx>
#include <Geom_BezierSurface.hxx>
#include <Geom_Surface.hxx>
#include <Law_Interpol.hxx>
#include <Law_Linear.hxx>
#include <QMenu>
#include <QMouseEvent>
#include <QRubberBand>
#include <QStyleFactory>
#include <ShapeAnalysis_Surface.hxx>
#include <Standard_PrimitiveTypes.hxx>
#include <StdFail_NotDone.hxx>
#include <StdSelect_FaceFilter.hxx>
#include <StepGeom_HArray1OfCartesianPoint.hxx>
#include <StepGeom_Polyline.hxx>
#include <TColStd_Array1OfInteger.hxx>
#include <TColStd_Array1OfReal.hxx>
#include <TColgp_Array1OfPnt.hxx>
#include <TColgp_Array1OfPnt2d.hxx>
#include <TColgp_Array2OfPnt.hxx>
#include <TCollection_HAsciiString.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <V3d_Coordinate.hxx>
#include <V3d_Viewer.hxx>
#include <gp_Pnt.hxx>
#include <gp_Pnt2d.hxx>
#include <math.hxx>

// occ header files.
#include <Aspect_Handle.hxx>
#include <V3d_Viewer.hxx>

#include "occSnippets.h"
#include <AIS_LengthDimension.hxx>
#include <AIS_Manipulator.hxx>
#include <AIS_RubberBand.hxx>
#include <AIS_Shape.hxx>
#include <AIS_ViewController.hxx>
#include <AIS_ViewCube.hxx>
#include <ElCLib.hxx>
#include <Graphic3d_Camera.hxx>
#include <Graphic3d_GraphicDriver.hxx>
#include <Graphic3d_Vec.hxx>
#include <V3d_RectangularGrid.hxx>
//
//
#include "snapobject.h"
#include <AIS_Line.hxx>
#include <AIS_TextLabel.hxx>
#include <Geom_Point.hxx>

#ifdef WNT
#include <WNT_Window.hxx>
#elif defined(__APPLE__) && !defined(MACOSX_USE_GLX)
#include <Cocoa_Window.hxx>
#else
#undef Bool
#undef CursorShape
#undef None
#undef KeyPress
#undef KeyRelease
#undef FocusIn
#undef FocusOut
#undef FontChange
#undef Expose
#include <Xw_Window.hxx>
#endif

static Handle(Graphic3d_GraphicDriver) & GetGraphicDriver() {
  static Handle(Graphic3d_GraphicDriver) aGraphicDriver;
  return aGraphicDriver;
}

MyGLView::MyGLView(QWidget *parent)
    : QGLWidget(parent), myXmin(0), myYmin(0), myXmax(0), myYmax(0),
      myCurrentMode(CurAction3d_DynamicRotation),
      myDegenerateModeIsOn(Standard_True), myRectBand(NULL) {
  // No Background
  setBackgroundRole(QPalette::NoRole);

  setAutoFillBackground(false);
  setAttribute(Qt::WA_NoSystemBackground);
  setAttribute(Qt::WA_OpaquePaintEvent);
  //   setAttribute( Qt::WA_WState_Visible, false );

  setBackgroundRole(QPalette::NoRole); // NoBackground );
  // set focus policy to threat QContextMenuEvent from keyboard
  setFocusPolicy(Qt::StrongFocus);
  setFocusPolicy(Qt::ClickFocus);

  // Enable the mouse tracking, by default the mouse tracking is disabled.
  setMouseTracking(true);

  myCurrentMode = CurAction3d_Nothing;

  MyMenu = new QMenu(this);
}

void MyGLView::init() {
  // Create Aspect_DisplayConnection
  Handle(Aspect_DisplayConnection) aDisplayConnection =
      new Aspect_DisplayConnection();

  // Get graphic driver if it exists, otherwise initialise it
  if (GetGraphicDriver().IsNull()) {
    GetGraphicDriver() = new OpenGl_GraphicDriver(aDisplayConnection);
  }

  // Get window handle. This returns something suitable for all platforms.
  //   WId window_handle = (WId) winId();

  WId window_handle = (WId)this->winId();

// Create appropriate window for platform
#ifdef WNT
  Handle(WNT_Window) wind = new WNT_Window((Aspect_Handle)window_handle);

#elif defined(__APPLE__) && !defined(MACOSX_USE_GLX)
  Handle(Cocoa_Window) wind = new Cocoa_Window((NSView *)window_handle);
#else
  Handle(Xw_Window) wind =
      new Xw_Window(aDisplayConnection, (Window)window_handle);
#endif

  // Create V3dViewer and V3d_View
  myViewer = new V3d_Viewer(GetGraphicDriver(), Standard_ExtString("viewer3d"));
  myViewer->SetDefaultBgGradientColors(
      Quantity_NOC_WHITE, Quantity_NOC_DODGERBLUE1, Aspect_GFM_VER);

  myView = myViewer->CreateView();

  myView->SetWindow(wind);
  if (!wind->IsMapped())
    wind->Map();

  // Create AISInteractiveContext
  myContext = new AIS_InteractiveContext(myViewer);

  // Set up lights etc
  myViewer->SetDefaultLights();
  myViewer->SetLightOn();

  myView->MustBeResized();
  //   myView->TriedronDisplay(Aspect_TOTP_LEFT_LOWER, Quantity_NOC_BLUE1, 0.1,
  //   V3d_ZBUFFER);

  myContext->SetDisplayMode(AIS_Shaded, Standard_True);
  myContext->DefaultDrawer()->SetFaceBoundaryDraw(true);
  //
  //    myView->SetBackgroundColor(Quantity_NOC_BLACK);

  myView->ZFitAll();
  myView->DepthFitAll();

  /////////////////////////////

  myRubberBand = new AIS_RubberBand();

  const Handle(Prs3d_Drawer) selectionStyle = new Prs3d_Drawer();
  selectionStyle->SetColor(
      static_cast<Quantity_NameOfColor>(Quantity_NOC_YELLOWGREEN));
  myContext->SetSelectionStyle(selectionStyle);

  myContext->MainSelector()->AllowOverlapDetection(true);

  Handle(AIS_ViewCube) aViewCube = new AIS_ViewCube();

  aViewCube->SetBoxColor(Quantity_NOC_GRAY50);
  aViewCube->SetContext(myContext);
  aViewCube->SetFixedAnimationLoop(true);
  aViewCube->SetSize(50);

  myContext->Display(aViewCube, true);

  myViewer->DisplayPrivilegedPlane(true, 1000);
  //
  firstPoint = gp_Pnt(0, 0, 0);
  lastPoint = gp_Pnt(0, 0, 1);

  Handle(Geom_Point) pnta = new Geom_CartesianPoint(firstPoint);
  Handle(Geom_Point) pntb = new Geom_CartesianPoint(lastPoint);

  //
  /*  gp_Pnt p(0,0,0);
    BRepBuilderAPI_MakePolygon
    polyBuilder(gp_Pnt(p.X()-snpRectSize/2,p.Y()+snpRectSize/2,p.Z()),
                                           gp_Pnt(p.X()+snpRectSize/2,p.Y()+snpRectSize/2,p.Z()),
                                           gp_Pnt(p.X()+snpRectSize/2,p.Y()-snpRectSize/2,p.Z()),
                                           gp_Pnt(p.X()-snpRectSize/2,p.Y()-snpRectSize/2,p.Z()),Standard_True);


    theSnapObject = new AIS_Shape(polyBuilder.Shape());
    theSnapObject->Attributes()->SetColor(Quantity_NOC_BLUE1);
    myContext->Display(theSnapObject,0,1,0);
 */

  myGrid = new customgrid(myContext, "0 10000 20000",
                          "0 6000 12000 18000 24000 30000", "0 7000 9000",
                          "A B C", "1 2 3 4 5 6", "Z1 Z2 Z3");
  myGrid->draw();
  tracker = new lineTracker(myContext, myView);
  copy_dialog = new copy_elements_dialog(this->parentWidget());

  mySnapObject = new snapObject(this, myContext, myView);
  mySnapObject->setGridSnapPoints(myGrid->getGridPoints());
  mySnapObject->setSnapMode(snapMode_End | snapMode_Mid);

  connect(this, &MyGLView::sendMouseMoveEvent, mySnapObject,
          &snapObject::mouseMoveEvent);

  connect(copy_dialog, &copy_elements_dialog::sendCopyRequest, this,
          &MyGLView::getCopyRequest);

  //

  connect(copy_dialog, &copy_elements_dialog::sendPickPointRequest, this,
          &MyGLView::getpickPointRequest);
}

Handle(AIS_InteractiveContext) & MyGLView::getContext() { return myContext; }

void MyGLView::paintEvent(QPaintEvent * /*theEvent*/) {
  if (myContext.IsNull()) {
    init();
  }

  foreach (QOCC_Object *o, Objects) { o->draw(); }
  myView->RedrawImmediate();
  viewPrecision(myView, true);
}

void MyGLView::resizeEvent(QResizeEvent * /*theEvent*/) {
  if (!myView.IsNull()) {
    myView->MustBeResized();
    viewPrecision(myView, true);
  }
}

void MyGLView::fitAll(void) {
  myView->FitAll();
  myView->ZFitAll();
  myView->RedrawImmediate();
  viewPrecision(myView, true);
}

void MyGLView::reset(void) {
  myView->Reset();
  viewPrecision(myView, true);
}

void MyGLView::pan(void) {
  myCurrentMode = CurAction3d_DynamicPanning;
  viewPrecision(myView, true);
}

void MyGLView::zoom(void) {
  myCurrentMode = CurAction3d_DynamicZooming;
  viewPrecision(myView, true);
}

void MyGLView::rotate(void) {
  myCurrentMode = CurAction3d_DynamicRotation;
  viewPrecision(myView, true);
}

void MyGLView::getpickPointRequest() { setMode(CurAction3d_GetpickPoint); }

void MyGLView::mousePressEvent(QMouseEvent *theEvent) {
  if (theEvent->button() == Qt::LeftButton) {
    onLButtonDown((theEvent->buttons() | theEvent->modifiers()),
                  theEvent->pos());
  } else if (theEvent->button() == Qt::MidButton) {
    onMButtonDown((theEvent->buttons() | theEvent->modifiers()),
                  theEvent->pos());
  } else if (theEvent->button() == Qt::RightButton) {
    onRButtonDown((theEvent->buttons() | theEvent->modifiers()),
                  theEvent->pos());
  }
}

void MyGLView::mouseReleaseEvent(QMouseEvent *theEvent) {

  if (theEvent->button() == Qt::LeftButton) {
    onLButtonUp(theEvent->buttons() | theEvent->modifiers(), theEvent->pos());
  } else if (theEvent->button() == Qt::MidButton) {
    onMButtonUp(theEvent->buttons() | theEvent->modifiers(), theEvent->pos());
  } else if (theEvent->button() == Qt::RightButton) {
    onRButtonUp(theEvent->buttons() | theEvent->modifiers(), theEvent->pos());
  }
}

void MyGLView::mouseMoveEvent(QMouseEvent *theEvent) {
  gp_Pnt P;

  P = theconvertToPlane(myView, theEvent->pos().x(),
                        theEvent->pos().y()); // theconvertToPlane

  Bar->showMessage(QString("X:%1 Y:%2 Z:%3").arg(P.X()).arg(P.Y()).arg(P.Z()));

  fireSnapMouseSignal(theEvent);

  OnMouseHover(theEvent->pos().x(), theEvent->pos().y(), P);
  onMouseMove(theEvent->buttons(), theEvent->pos());
}

void MyGLView::wheelEvent(QWheelEvent *theEvent) {
  onMouseWheel(theEvent->buttons(), theEvent->delta(), theEvent->pos());
}

void MyGLView::onLButtonDown(const int /*theFlags*/, const QPoint thePoint) {
  // Save the current mouse coordinate in min.
  myXmin = thePoint.x();
  myYmin = thePoint.y();
  myXmax = thePoint.x();
  myYmax = thePoint.y();
}

void MyGLView::onMButtonDown(const int /*theFlags*/, const QPoint thePoint) {
  // Save the current mouse coordinate in min.
  myXmin = thePoint.x();
  myYmin = thePoint.y();
  myXmax = thePoint.x();
  myYmax = thePoint.y();

  //    if (myCurrentMode == CurAction3d_DynamicRotation)
  { myView->StartRotation(thePoint.x(), thePoint.y()); }
}

void MyGLView::onRButtonDown(const int /*theFlags*/,
                             const QPoint /*thePoint*/) {
  if (myCurrentMode == CurAction3d_DynamicPanning)
    myCurrentMode = CurAction3d_DynamicRotation;
  else
    myCurrentMode = CurAction3d_DynamicPanning;
}

void MyGLView::onMouseWheel(const int /*theFlags*/, const int theDelta,
                            const QPoint thePoint) {
  Standard_Integer aFactor = 16;

  Standard_Integer aX = thePoint.x();
  Standard_Integer aY = thePoint.y();

  if (theDelta > 0) {
    aX += aFactor;
    aY += aFactor;
  } else {
    aX -= aFactor;
    aY -= aFactor;
  }

  //  myView->Zoom(thePoint.x(), thePoint.y(), aX, aY);

  QPoint p = thePoint;
  myView->StartZoomAtPoint(p.x(), p.y());
  double delta = (double)(theDelta) / (15 * 8);
  int x = p.x();
  int y = p.y();
  int x1 = (int)(p.x() + width() * delta / 100);
  int y1 = (int)(p.y() + height() * delta / 100);
  myView->ZoomAtPoint(x, y, x1, y1);
}

void MyGLView::addItemInPopup(QAction *theMenu) { MyMenu->addAction(theMenu); }

void MyGLView::popup(const int x, const int y) {
  MyMenu->exec(mapToGlobal(QPoint(x, y)));
}

void MyGLView::onLButtonUp(const int theFlags, const QPoint thePoint) {
  // Hide the QRubberBand

  if (myCurrentMode == CurAction3d_DrawElement ||
      myCurrentMode == CurAction3d_SetWorkPlane2Points ||
      myCurrentMode == CurAction3d_GetpickPoint) {
    lClickCount++;
    switch (lClickCount) {
    case 1: {
      if (mySnapObject->getSnappedPoint(firstPoint)) {
      } else
        firstPoint = mySnapObject->getWorldPoint(thePoint);
      myClickStatus = clickStatusLeftFirstClick;

      mySnapObject->activate();
      tracker->startTracking(firstPoint);

    } break;
    case 2: {
      if (mySnapObject->getSnappedPoint(lastPoint)) {
      } else
        lastPoint = mySnapObject->getWorldPoint(thePoint);

      if (myCurrentMode == CurAction3d_DrawElement) {
        QOCC_ipe_Profile *Prf = new QOCC_ipe_Profile(
            myContext, "IPE400", firstPoint, lastPoint, true);
        addObject(Prf);
        fireButtonStatus(0, false);
      }

      if (myCurrentMode == CurAction3d_SetWorkPlane2Points) {
        setWorkPlane(firstPoint, lastPoint);
      }

      if (myCurrentMode == CurAction3d_GetpickPoint) {
        gp_Pnt df;
        df.SetX(lastPoint.X() - firstPoint.X());
        df.SetY(lastPoint.Y() - firstPoint.Y());
        df.SetZ(lastPoint.Z() - firstPoint.Z());
        copy_dialog->setPickedPoint(df);
      }

      myCurrentMode = CurAction3d_Nothing;
      myClickStatus = clickStatusLeftLastClick;
      lClickCount = lClickCount % 2;
      myContext->SetAutomaticHilight(true);
      mySnapObject->deactivate();
      tracker->finishTracking();

    } break;
    default:
      break;
    }
  }

  DrawRectangleRubberBand(false);

  // Ctrl for multi selection.
  if (thePoint.x() == myXmin && thePoint.y() == myYmin) {
    if (theFlags & Qt::ControlModifier) {
      multiInputEvent(thePoint.x(), thePoint.y());
    } else {
      //  inputEvent(thePoint.x(), thePoint.y());
    }
  }
  //
  myView->Redraw();
}

void MyGLView::onMButtonUp(const int /*theFlags*/, const QPoint thePoint) {
  if (thePoint.x() == myXmin && thePoint.y() == myYmin) {
    panByMiddleButton(thePoint);
  }
}

void MyGLView::onRButtonUp(const int /*theFlags*/, const QPoint thePoint) {
  popup(thePoint.x(), thePoint.y());
}

void MyGLView::onMouseMove(const int theFlags, const QPoint thePoint) {

  curSPt = mySnapObject->getWorldPoint(thePoint);

  if (tracker->active)
    tracker->setEnd(curSPt);

  // Draw the rubber band.
  if (theFlags & Qt::LeftButton) {
    DrawRectangleRubberBand(true);
    myXmax = thePoint.x();
    myYmax = thePoint.y();

    dragEvent(thePoint.x(), thePoint.y());
  }

  // Ctrl for multi selection.
  if (theFlags & Qt::ControlModifier) {
    multiMoveEvent(thePoint.x(), thePoint.y());
  } else {
    moveEvent(thePoint.x(), thePoint.y());
  }

  // Middle button.
  if (theFlags & Qt::MidButton) {
    /*
    switch (myCurrentMode)
    {
    case CurAction3d_DynamicRotation:
        myView->Rotation(thePoint.x(), thePoint.y());
        break;

    case CurAction3d_DynamicZooming:
        myView->Zoom(myXmin, myYmin, thePoint.x(), thePoint.y());
        break;

    case CurAction3d_DynamicPanning:
        myView->Pan(thePoint.x() - myXmax, myYmax - thePoint.y());
        myXmax = thePoint.x();
        myYmax = thePoint.y();
        break;
    default:
        break;
    }
*/
    myView->Rotation(thePoint.x(), thePoint.y());
  }
}

void MyGLView::keyPressEvent(QKeyEvent *event) {
  if (event->key() == Qt::Key_R)
    myCurrentMode = CurAction3d_DynamicRotation;
  if (event->key() == Qt::Key_P)
    myCurrentMode = CurAction3d_DynamicPanning;
  if (event->key() == Qt::Key_Delete)
    deleteSelectedObjects();
}

void MyGLView::dragEvent(const int x, const int y) {
  myContext->Select(myXmin, myYmin, x, y, myView, Standard_True);

  emit selectionChanged();
}

void MyGLView::multiDragEvent(const int x, const int y) {
  myContext->ShiftSelect(myXmin, myYmin, x, y, myView, Standard_True);

  emit selectionChanged();
}

void MyGLView::inputEvent(const int x, const int y) {
  Q_UNUSED(x);
  Q_UNUSED(y);

  myContext->Select(Standard_True);

  emit selectionChanged();
}

void MyGLView::multiInputEvent(const int x, const int y) {
  Q_UNUSED(x);
  Q_UNUSED(y);

  myContext->ShiftSelect(Standard_True);

  emit selectionChanged();
}

void MyGLView::moveEvent(const int x, const int y) {
  myContext->MoveTo(x, y, myView, Standard_True);
}

void MyGLView::multiMoveEvent(const int x, const int y) {
  myContext->MoveTo(x, y, myView, Standard_True);
}

void MyGLView::panByMiddleButton(const QPoint &thePoint) {
  Standard_Integer aCenterX = 0;
  Standard_Integer aCenterY = 0;

  QSize aSize = size();

  aCenterX = aSize.width() / 2;
  aCenterY = aSize.height() / 2;

  myView->Pan(aCenterX - thePoint.x(), thePoint.y() - aCenterY);
}

gp_Pnt MyGLView::selectionChanged() {
  QElapsedTimer timer;
  timer.start();

  gp_Pnt myPoint;
  selectedObjects.clear();
  int numselected = 0;

  myContext->InitSelected();
  while (myContext->MoreSelected()) {
    if (myContext->HasSelectedShape()) {
      if (myContext->SelectedShape().ShapeType() == TopAbs_VERTEX) {
      } else {
        if (myContext->SelectedShape().ShapeType() == TopAbs_SOLID) {
          TopoDS_Shape sShape = myContext->SelectedShape();
          //
          foreach (QOCC_Object *o, Objects) {
            if (o->objType == 100) {
              TopoDS_Shape aShape =
                  dynamic_cast<QOCC_ipe_Profile *>(o)->getAISShape()->Shape();
              if (sShape.IsSame(aShape)) {
                selectedObjects.push_back(o);
                numselected++;
              }
            }
          }
        }
      }

      myContext->NextSelected();
    }
    //   myContext->ClearSelected(true);
  }

  qDebug() << "The selection changed operation took" << timer.elapsed()
           << "milliseconds";

  return myPoint;
}

void MyGLView::addObject(QOCC_Object *o) {
  Objects.push_back(o);
  //  this->update();
}

void MyGLView::copyObjects(QVector<QOCC_Object *> Objects, gp_Pnt cparameters,
                           int no) {
  int counter = 0;

  QElapsedTimer timer;

  QProgressDialog progress("Copying objects...", "Abort Copy", 0,
                           Objects.size() * no, this);
  progress.setWindowModality(Qt::WindowModal);

  foreach (QOCC_Object *O, Objects) {

    switch (O->objType) {
    case 0: {
      //
    }

    break;
    case 100:

      for (int i = 1; i <= no; i++) {
        gp_Pnt P1, P2;
        QString ps = dynamic_cast<QOCC_ipe_Profile *>(O)->getProfileName();
        dynamic_cast<QOCC_ipe_Profile *>(O)->getPoints(P1, P2);

        P1.SetX(P1.X() + cparameters.X() * i);
        P1.SetY(P1.Y() + cparameters.Y() * i);
        P1.SetZ(P1.Z() + cparameters.Z() * i);
        //
        P2.SetX(P2.X() + cparameters.X() * i);
        P2.SetY(P2.Y() + cparameters.Y() * i);
        P2.SetZ(P2.Z() + cparameters.Z() * i);

        timer.start();

        QOCC_ipe_Profile *newObj =
            new QOCC_ipe_Profile(myContext, ps, P1, P2, false);
        newObj->setOrientation(
            dynamic_cast<QOCC_ipe_Profile *>(O)->getOrientation());
        addObject(newObj);
        qDebug() << "The copy operation took" << timer.elapsed()
                 << "milliseconds";

        progress.setValue(counter++);
      }
      break;
    default:
      break;
    }
  }

  //   myView->Redraw();
}

void MyGLView::setMode(CurrentAction3d aMode) {
  myCurrentMode = aMode;

  switch (myCurrentMode) {
  case CurAction3d_DrawElement:
    myContext->SetAutomaticHilight(false);
    mySnapObject->activate();
    break;

  case CurAction3d_SetWorkPlane2Points:
    myContext->SetAutomaticHilight(false);
    mySnapObject->activate();
    break;
  case CurAction3d_GetpickPoint:
    myContext->SetAutomaticHilight(false);
    mySnapObject->activate();
    break;
  case CurAction3d_Nothing:
    mySnapObject->deactivate();
    break;
  default:
    break;
  }
}

void MyGLView::setViewType(ViewType aViewType) {
  switch (aViewType) {
  case ViewType_Front: {
    myView->SetProj(V3d_Yneg);
    myViewer->SetPrivilegedPlane(gp_Ax3(
        gp_Pnt(0, 0, 0), gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(0, 1, 0)))));
    myGrid->setGridPlane(drawPlaneXOZ);
  } break;
  case ViewType_Back: {
    myView->SetProj(V3d_Ypos);
    myViewer->SetPrivilegedPlane(gp_Ax3(
        gp_Pnt(0, 0, 0), gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(0, 0, -1)))));
    myGrid->setGridPlane(drawPlaneXOZ);
  } break;
  case ViewType_Top: {
    myView->SetProj(V3d_Zpos);
    myViewer->SetPrivilegedPlane(gp_Ax3(
        gp_Pnt(0, 0, 0), gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(0, 0, 1)))));
    myGrid->setGridPlane(drawPlaneXOY);
  } break;
  case ViewType_Bottom: {
    myView->SetProj(V3d_Zneg);
    myViewer->SetPrivilegedPlane(gp_Ax3(
        gp_Pnt(0, 0, 0), gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(0, 0, -1)))));
    myGrid->setGridPlane(drawPlaneXOY);
  } break;
  case ViewType_Left: {
    myView->SetProj(V3d_Xneg);
    myViewer->SetPrivilegedPlane(gp_Ax3(
        gp_Pnt(0, 0, 0), gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(-1, 0, 0)))));
    myGrid->setGridPlane(drawPlaneYOZ);
  } break;
  case ViewType_Right: {
    myView->SetProj(V3d_Xpos);
    myViewer->SetPrivilegedPlane(gp_Ax3(
        gp_Pnt(0, 0, 0), gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(1, 0, 0)))));
    myGrid->setGridPlane(drawPlaneYOZ);
  } break;
  case ViewType_3D: {
    myView->SetProj(V3d_XposYnegZpos);
    myViewer->SetPrivilegedPlane(gp_Ax3(
        gp_Pnt(0, 0, 0), gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(0, 0, 1)))));
    myGrid->setGridPlane(drawPlaneXOY);
  } break;
  default:
    break;
  }

  fitAll();
}

void MyGLView::addGrid(gp_Pnt aOrigin, QString xGridValues, QString yGridValues,
                       QString zGridValues, QString xGridLabels,
                       QString yGridLabels, QString zGridLabels) {
  myGrid->setGrid(aOrigin, xGridValues, yGridValues, zGridValues, xGridLabels,
                  yGridLabels, zGridLabels);

  mySnapObject->setGridSnapPoints(myGrid->getGridPoints());

  fitAll();
}

void MyGLView::setWorkPlane(gp_Ax3 aPlane) {
  myViewer->SetPrivilegedPlane(aPlane);
}

void MyGLView::setWorkPlane(gp_Pnt p1, gp_Pnt p2, gp_Pnt p3) {
  gp_Dir newXDir;
  gp_Dir newYDir;
  gp_Dir newZDir;
  gp_Pnt tp;

  //   myViewer->SetPrivilegedPlane(gp::XOY());

  newXDir = gp_Dir(gp_Vec(p1, p2));
  newYDir = gp_Dir(gp_Vec(p1, p3));
  newZDir = newXDir;
  newZDir.Rotate(gp_Ax1(tp, newYDir), DEG(90));

  gp_Ax3 plane;
  plane.SetLocation(p1);
  plane.SetDirection(newZDir);
  plane.SetXDirection(newXDir);
  plane.SetYDirection(newYDir);

  // previliged plane declared

  myViewer->SetPrivilegedPlane(plane);
  // previliged plane setted

  workPlane = plane;

  myView->Redraw();
}

void MyGLView::setWorkPlane(gp_Pnt p1, gp_Pnt p2) {
  gp_Pnt p3;
  gp_Dir newXDir;
  gp_Dir newYDir;
  gp_Dir newZDir;
  gp_Dir rotateAxe;

  if (p1.X() - p2.X() == 0)
    rotateAxe = gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(0, 0, 1)));
  else if (p1.Y() - p2.Y() == 0)
    rotateAxe = gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(0, 0, 1)));
  else if (p1.Z() - p2.Z() == 0)
    rotateAxe = gp_Dir(gp_Vec(gp_Pnt(0, 0, 0), gp_Pnt(0, 1, 0)));

  p3 = p2.Rotated(gp_Ax1(p1, rotateAxe), DEG(90));
  p3.SetZ(p1.Z());

  //
  newXDir = gp_Dir(gp_Vec(p1, p2));
  newYDir = gp_Dir(gp_Vec(p1, p3));
  newZDir = newXDir;
  newZDir.Rotate(gp_Ax1(p1, newYDir), DEG(90));
  newZDir.Reverse();

  gp_Ax3 plane;
  plane.SetLocation(p1);
  plane.SetDirection(newZDir);  // Z
  plane.SetXDirection(newXDir); // X
  plane.SetYDirection(newYDir); // Y

  // previliged plane declared

  myViewer->SetPrivilegedPlane(plane);
  // previliged plane setted

  workPlane = plane;

  myView->Redraw();
}

void MyGLView::setViewToWorkPlane() {
  gp_Pnt newOrigin = workPlane.Location();
  gp_Pnt newUpPnt = newOrigin.Translated(workPlane.Direction());
  newUpPnt.SetX(newUpPnt.X() * 100);
  newUpPnt.SetY(newUpPnt.Y() * 100);
  newUpPnt.SetZ(newUpPnt.Z() * 100);

  myView->Camera()->SetEye(newUpPnt);
  myView->Camera()->SetDirection(workPlane.Direction().Reversed());
  myView->Camera()->SetUp(workPlane.YDirection());
  myView->Redraw();
}

void MyGLView::setPreviligedPlaneToCamera() {
  gp_Pnt p3;
  gp_Dir newYDir;
  gp_Dir newZDir;
  gp_Ax3 newPre;

  newZDir = myView->Camera()->Direction();
  newYDir = myView->Camera()->Up();
  p3 = gp::Origin();

  newPre = gp_Ax3(p3, newZDir);
  newPre.SetXDirection(newYDir);

  myViewer->SetPrivilegedPlane(newPre);
}

void MyGLView::OnMouseHover(Standard_Real x, Standard_Real y, gp_Pnt &P) {

  myContext->Deactivate();
  myContext->Activate(AIS_Shape::SelectionMode(TopAbs_SOLID));

  myContext->MoveTo(x, y, myView, true);

  if (myContext->HasDetected()) {
    myContext->InitDetected();

    do {
      Handle_AIS_InteractiveObject hObj = myContext->DetectedInteractive();
      if (!hObj.IsNull()) {
        // Access the highlighted object
        Handle(AIS_Shape) hShape = Handle(AIS_Shape)::DownCast(hObj);
        if (hShape.IsNull()) // not AIS_Shape
        {
          return;
        }

        TopoDS_Shape shape = hShape->Shape(); // Get TopoDS_Shape (geometry)
        if (shape.ShapeType() == TopAbs_VERTEX) {
          const TopoDS_Vertex &vertex = TopoDS::Vertex(shape);
          P = BRep_Tool::Pnt(vertex);

          curSPt = P;
        } else if (shape.ShapeType() == TopAbs_WIRE) {

        } else if (shape.ShapeType() == TopAbs_SOLID) {
          TopoDS_Shape sShape = hShape->Shape();
          mySnapObject->setSelectedShape(sShape);
        }
      }
      myContext->NextDetected();

    }

    while (myContext->MoreDetected());
  }
}

void MyGLView::DrawRectangleRubberBand(Standard_Boolean theToDraw) {

  const Handle(AIS_InteractiveContext) &aCtx = myContext;
  if (!theToDraw) {
    aCtx->Remove(myRubberBand, false);
    aCtx->CurrentViewer()->RedrawImmediate();
    return;
  }

  QRect aRect;

  aRect.setRect(0, 0, size().width(), size().height());
  myRubberBand->SetLineType(Aspect_TOL_SOLID);
  myRubberBand->SetFillColor(Quantity_NOC_GREEN2);
  myRubberBand->SetFilling(true);
  myRubberBand->SetFillTransparency(0.8);
  myRubberBand->SetRectangle(myXmin, aRect.height() - myYmin, myXmax,
                             aRect.height() - myYmax);
  if (!aCtx->IsDisplayed(myRubberBand)) {
    aCtx->Display(myRubberBand, false);
  } else {
    aCtx->Redisplay(myRubberBand, false);
  }
  aCtx->CurrentViewer()->RedrawImmediate();
}

void MyGLView::deleteSelectedObjects() {
  for (myContext->InitSelected(); myContext->MoreSelected();
       myContext->InitSelected()) {
    myContext->Remove(myContext->SelectedInteractive(), Standard_True);
  }
}

void MyGLView::getCopyRequest(gp_Pnt &p, int no) {
  // copy the objects
  copyObjects(selectedObjects, p, no);
}

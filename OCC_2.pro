QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS \
WNT

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    axisform.cpp \
    copyElementsDialog.cpp \
    customgrid.cpp \
    linetracker.cpp \
    main.cpp \
    mainwindow.cpp \
    myglview.cpp \
    occObjects.cpp \
    occSnippets.cpp \
    profileform.cpp \
    snapobject.cpp

HEADERS += \
    axisform.h \
    copyElementsDialog.h \
    customgrid.h \
    linetracker.h \
    mainwindow.h \
    myglview.h \
    occObjects.h \
    occSnippets.h \
    profileform.h \
    snapobject.h

FORMS += \
    axisform.ui \
    copyElementsDialog.ui \
    mainwindow.ui \
    profileform.ui


INCLUDEPATH +=  \
D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/inc \


_OCC_LIB = "D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/bind"

LIBS+= -L$${_OCC_LIB}


LIBS +=         \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKBin                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKBinL                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKBinTObj               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKBinXCAF               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKBO                    \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKBool                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKBRep                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKCAF                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKCDF                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKDCAF                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKDraw                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKernel                 \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKFeat                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKFillet                \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKG2d                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKG3d                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKGeomAlgo              \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKGeomBase              \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKHLR                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKIGES                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKLCAF                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKMath                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKMesh                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKMeshVS                \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKOffset                \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKOpenGl                \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKPrim                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKQADraw                \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKRWMesh                \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKService               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKShHealing             \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKStd                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKStdL                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKSTEP                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKSTEP209               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKSTEPAttr              \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKSTEPBase              \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKSTL                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKTObj                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKTObjDRAW              \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKTopAlgo               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKTopTest               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKV3d                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKVCAF                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKViewerTest            \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKVRML                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXCAF                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXDEDRAW               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXDEIGES               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXDESTEP               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXMesh                 \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXml                   \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXmlL                  \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXmlTObj               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXmlXCAF               \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXSBase                \
-L"D:/OpenCASCADE-7.4.0-vc14-64/opencascade-7.4.0/build2/win64/vc14/libd" -lTKXSDRAW                \




# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    myresources.qrc

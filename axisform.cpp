#include "axisform.h"
#include "ui_axisform.h"
#include "myglview.h"

axisForm::axisForm(QWidget *parent) :
                                      QWidget(parent),
                                      ui(new Ui::axisForm)
{
    ui->setupUi(this);
}

axisForm::~axisForm()
{
    delete ui;
}

void axisForm::fireAddNewAxis()
{

    QString xGridValues = ui->lineEdit->text();
    QString yGridValues = ui->lineEdit_2->text();
    QString zGridValues = ui->lineEdit_3->text();
    QString xGridLabels = ui->lineEdit_4->text();
    QString yGridLabels = ui->lineEdit_5->text();
    QString zGridLabels = ui->lineEdit_6->text();

    gp_Pnt aOrigin(ui->lineEdit_7->text().toFloat(),ui->lineEdit_8->text().toFloat(),ui->lineEdit_9->text().toFloat());


    emit sendAddNewAxis( aOrigin,         // send signal
                         xGridValues,
                         yGridValues,
                         zGridValues,
                         xGridLabels,
                         yGridLabels,
                         zGridLabels);
}

void axisForm::on_pushButton_clicked()
{
  fireAddNewAxis();
}

void axisForm::on_pushButton_2_clicked()
{
    this->close();
}
